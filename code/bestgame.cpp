
#define DEBUG_FONT_NAME "Envy Code R"
#define DEBUG_FONT_RES 512

#include "bestgame.h"

internal void 
InitInput_bags (Input_bags *input)
{
	input->keys_on_watch = AllocateFromMemoryPool_bags(&game_memory.permanent, KeyPress_bags, MAX_KEYS_ON_WATCH);
	input->keys_on_watch_count = 0;

	WatchKey_bags (input, KEY_UP_ARROW);
	WatchKey_bags (input, KEY_DOWN_ARROW);
	WatchKey_bags (input, KEY_LEFT_ARROW);
	WatchKey_bags (input, KEY_RIGHT_ARROW);

	WatchKey_bags (input, KEY_W);
	WatchKey_bags (input, KEY_A);
	WatchKey_bags (input, KEY_S);
	WatchKey_bags (input, KEY_D);
	WatchKey_bags (input, KEY_SPACE);

	WatchKey_bags (input, KEY_F1);
	WatchKey_bags (input, KEY_F4);
	WatchKey_bags (input, KEY_F11);
	WatchKey_bags (input, KEY_L_ALT);
	WatchKey_bags (input, KEY_L_CTRL);

	WatchKey_bags (input, KEY_LEFT_MOUSE_BUTTON);
	WatchKey_bags (input, KEY_RIGHT_MOUSE_BUTTON);
}

internal void
InitGame (GameState *game_state, RendererState_bags *renderer_state, AssetManagerState_bags *asset_state,
		  SoundState_bags *sound_state)
{
	SGW_bags *world = &game_state->world;
	if(!world->loaded)
		*world = *LoadEntireSGW_bags(asset_state, &game_memory.permanent);
	world->gravity_type = SGW_GRAVITYTYPE_LOCAL;

	SGWRegion_bags *first_region = SGW_GetRegion_bags(world, world->first_region_p);

	Hero *hero = &game_state->hero;
	if(first_region)
	{
		hero->wp = first_region->p;
		hero->p = world->first_p;
	}
	else
	{
		hero->wp = {0,0,0};
		hero->p = V2(0.0f,0.0f);

		ShowSystemPopupTextWindow_bags ("Erro", "Mundo invalido!");
	}
	hero->v = V2(0.0f,0.0f);
	hero->a = V2(0.0f,0.0f);
	hero->r = 0.0f;

	world->current_region = SGW_GetCurrentRegion_bags(world, &hero->wp, &hero->p, HERO_SIZE);

	Camera *camera = &renderer_state->camera;
	camera->p = SGW_GetAbsolutePosition_bags(world, hero->wp, hero->p) + V3(0.0f,0.0f,1.0f);
	camera->focus = camera->p - V3(0.0f,0.0f,1.0f);
	camera->up = V3(0.0f,1.0f,0.0f);

	if(!renderer_state->font_bag.loaded)
		renderer_state->font_bag = GetFontBAG_bags("data\\fonts.bag", &game_memory.assets, &game_memory.transient);
	renderer_state->debug_font = renderer_state->font_bag.fonts[GetFontIndex_bags(renderer_state->font_bag,DEBUG_FONT_NAME)];
	renderer_state->debug_font_res = DEBUG_FONT_RES;

	if(!sound_state->loaded_sounds)
	{
		game_state->bgm_id = debug_LoadEntireSound_bags(sound_state, "data\\raw\\audio\\bgm_late_afternoon_drifting.wav", &game_memory.assets, &game_memory.transient);
		// PlaySoundFromPoint_bags (sound_state, game_state->bgm_id, true, 1.0f, 1.0f, V2(0.0f,0.0f), &game_memory.permanent);
		// PlaySound_bags(sound_state, game_state->bgm_id, true, 0.7f, 0.7f, &game_memory.permanent);
		game_state->sfx_id  = debug_LoadEntireSound_bags(sound_state, "data\\raw\\audio\\sfx_sword_swing.wav", &game_memory.assets, &game_memory.transient);
		game_state->sfx_id1 = debug_LoadEntireSound_bags(sound_state, "data\\raw\\audio\\sfx_ah_ah_ah.wav", &game_memory.assets, &game_memory.transient);
	}

	r32 hw = (world->width*0.5f) * SGWREGION_WIDTH;
	r32 hh = (world->height*0.5f) * SGWREGION_HEIGHT;
	r32 hd = (world->depth*0.5f) * SGWREGION_DEPTH;
	game_state->p0 = V3(-hw,-hh,hd);
	game_state->p1 = V3(-hw,+hh,hd);
	game_state->p2 = V3(+hw,+hh,hd);
	game_state->p3 = V3(+hw,-hh,hd);
	game_state->point_holding = -1;

	game_state->initialized = true;
}

internal void
UpdateAndRenderGame (Input_bags *input, GameState *game_state, RendererState_bags *renderer_state, 
					 AssetManagerState_bags *asset_state, SoundState_bags *sound_state, 
					 System_bags *system, r32 dt)
{
	if(!game_state->initialized)
		InitGame(game_state, renderer_state, asset_state, sound_state);

	SGW_bags *world = &game_state->world;
	if(!world->loaded) return;

	Hero *hero = &game_state->hero;
	Camera *camera = &renderer_state->camera;

	// camera
	{
		V3 move = {};
		if(IsKeyDown_bags(input, KEY_L_CTRL))
		{
			move.z = (IsKeyDown_bags(input, KEY_DOWN_ARROW) ? 1.0f : 0.0f) - (IsKeyDown_bags(input, KEY_UP_ARROW) ? 1.0f : 0.0f);
		}
		else
		{
			move = V3((IsKeyDown_bags(input, KEY_RIGHT_ARROW) ? 1.0f : 0.0f) - (IsKeyDown_bags(input, KEY_LEFT_ARROW) ? 1.0f : 0.0f),
					  (IsKeyDown_bags(input, KEY_UP_ARROW) ? 1.0f : 0.0f)    - (IsKeyDown_bags(input, KEY_DOWN_ARROW) ? 1.0f : 0.0f), 
					  0.0f);
			Normalize(&move);
		}

		move = move * 5.0f * dt;
		camera->p = camera->p + move;
		// camera->p = GetAbsolutePosition_bags(world, hero->wp, hero->p)*(1.0f/3.0f) + V3(0.0f,0.0f,1.0f);
		camera->focus = camera->p - V3(0.0f,0.0f,1.0f);
		// UpdateCameraSectionBound_bags(world, camera);
	}
	r32 top     = +1.0f * camera->p.z;
	r32 bottom  = -1.0f * camera->p.z;
	r32 left    = -renderer_state->window_ratio * camera->p.z;
	r32 right   = +renderer_state->window_ratio * camera->p.z;

	// if(WasKeyJustPressed_bags(input, KEY_LEFT_MOUSE_BUTTON))
	// 	PlaySound_bags(sound_state, game_state->sfx_id, false, 0.5f, 0.5f, &game_memory.permanent);
	if(WasKeyJustPressed_bags(input, KEY_RIGHT_MOUSE_BUTTON))
		PlaySound_bags(sound_state, game_state->sfx_id1, false, 1.5f, 1.5f, &game_memory.permanent);

	RendererEnableDepth_bags();

	Mat4 mvp = {};
	Mat4 model_matrix = {};
	Identity(&model_matrix);
	
	{
		u32 width, height;
		GetScreenSize_bags(system, &width,&height);
		RendererResize_bags(renderer_state, 0,0,width,height);
	}

	Mat4 view_matrix = {};
	Identity(&view_matrix);
	SetLookAt (&view_matrix, 
				camera->p.x, camera->p.y, camera->p.z,  
				camera->focus.x, camera->focus.y, camera->focus.z,  
				camera->up.x, camera->up.y, camera->up.z);
	// ApplyTransforms(&model_matrix, camera->p, {1.0f,1.0f,1.0f}, QUAT_ZERO);
	view_matrix = view_matrix * model_matrix;
	renderer_state->world_view_matrix = view_matrix;
	
	Mat4 projection_matrix = {};
	Identity(&projection_matrix);
	SetOrthographicProjection(&projection_matrix,
								left, right, 
								bottom, top,
								0.0f, 10.0f);
	// SetPerspectiveProjection (&projection_matrix, 0.0f, 1.0f, 90.0f, renderer_state->window_ratio);
	renderer_state->world_projection_matrix = projection_matrix;
	
	// heroi
	{
		V2 move = V2((IsKeyDown_bags(input, KEY_D) ? 1.0f : 0.0f) - (IsKeyDown_bags(input, KEY_A) ? 1.0f : 0.0f),
					  (IsKeyDown_bags(input, KEY_W) ? 1.0f : 0.0f)    - (IsKeyDown_bags(input, KEY_S) ? 1.0f : 0.0f));
		Normalize(&move);
		move = move * 3.0f * dt;
		V2 forces = move;
		SGW_ApplyGravity_bags(world, hero->wp, hero->p, 1.0f, &forces, dt);
		hero->p = hero->p + forces;
		SGW_GetCurrentRegion_bags(world, &hero->wp, &hero->p, HERO_SIZE);

		V3 hero_abs_p = SGW_GetAbsolutePosition_bags(world, hero->wp, hero->p);
		{
			V3 cr_abs_p = SGW_GetAbsolutePosition_bags(world, world->current_region->p, V2(0.0f,0.0f));

			char txt[256];
			sprintf(txt, "-hero: r[%.2f,%.2f] a[%.2f,%.2f,%.2f] w[%d,%d,%d]\n-camera: a[%.2f,%.2f,%.2f]\n-current region: a[%.2f,%.2f,%.2f] w[%d,%d,%d]", 
					hero->p.x,hero->p.y, 
					hero_abs_p.x, hero_abs_p.y,hero_abs_p.z,
					hero->wp.x,hero->wp.y,hero->wp.z,

					camera->p.x,camera->p.y,camera->p.z,

					cr_abs_p.x, cr_abs_p.y, cr_abs_p.z,
					world->current_region->p.x, world->current_region->p.y, world->current_region->p.z);
			RenderText_bags(renderer_state, &renderer_state->debug_font, 
							renderer_state->gui_projection_matrix, 
							txt, 
							DEBUG_FONT_RES, 0.15f, V2(0.0f,0.0f), 
							V3(1.0f,0.2f,0.2f), 1.0f, 
							&game_memory.permanent, &game_memory.transient);
		}
		SetHearingPoint_bags(sound_state, hero_abs_p.xy);

		// camera->p = V2_V3(p.xy,camera->p.z);

		ApplyTransforms (&model_matrix, hero_abs_p, V3(HERO_WIDTH,HERO_HEIGHT,1.0f), QUAT_ZERO);
		mvp = (model_matrix * view_matrix) * projection_matrix;
		{
			V3 positions[] = {V3(-0.5f,0.5f,0.0f), V3(0.5f,0.5f,0.0f), V3(0.5f,-0.5f,0.0f), V3(-0.5f,-0.5f,0.0f)};
			V3 colors[4];
			SET_COLORS(colors,V3(1.0f,1.0f,1.0f),4);
			RenderSolidImmediate_bags (renderer_state, mvp, 1.0f, positions, colors, 4, 0, 0, LINE_LOOP);
		}
	}

	// Level
	// SGW_UpdateAndRender_bags(renderer_state, world, dt);

	mvp = view_matrix * projection_matrix;
	{
		V3 positions[] = 
		{
			V2_V3(GUI_TL(renderer_state->window_ratio),0.0f)*3.0f + V2_V3(camera->p.xy,0.0f), 
			V2_V3(GUI_TR(renderer_state->window_ratio),0.0f)*3.0f + V2_V3(camera->p.xy,0.0f), 
			V2_V3(GUI_BR(renderer_state->window_ratio),0.0f)*3.0f + V2_V3(camera->p.xy,0.0f), 
			V2_V3(GUI_BL(renderer_state->window_ratio),0.0f)*3.0f + V2_V3(camera->p.xy,0.0f)
		};
		V3 colors[4];
		SET_COLORS(colors,V3(0.2f,0.2f,1.0f),4);
		RendererSetLineWidth_bags(6.0f);
		RenderSolidImmediate_bags (renderer_state, mvp, 1.0f, positions, colors, 4, 0, 0, LINE_LOOP);
		RendererSetLineWidth_bags(1.0f);
	}

	ApplyTransforms (&model_matrix, V3(0.0f,0.0f,0.0f), V3(1.0f,1.0f,1.0f), QUAT_ZERO);
	mvp = (model_matrix * view_matrix) * projection_matrix;
	{
		const u32 res = 16;
		const u32 point_count = (res*2)-2;
		V3 positions[point_count];		
		u32 count = GenerateCubicBezier3D(game_state->p0, game_state->p1, game_state->p2, game_state->p3, res, positions);
		V3 colors[point_count];
		SET_COLORS(colors,V3(1.0f,1.0f,1.0f),res);
		SET_COLORS((colors+res),V3(0.0f,0.0f,1.0f),res-2);
		V3 a = game_state->p0;
		V3 b = game_state->p3;
		V3 ab = b-a;
		i32 i = 0;
		r32 dt = 1.0f/res;
		for (r32 t = dt; t < 1.0f; t += dt)
		{
			V3 p = positions[1+i];
			positions[(res)+i] = a + (ab*t);
			++i;
		}
		const u32 index_count = 1;
		u16 indices[index_count];
		// RenderSolidImmediate_bags (renderer_state, mvp, 1.0f, positions, colors, point_count, 0, 0, POINTS_);
	}
	{
		Pointer_bags mouse = GetMousePointer_bags(input);
	/*

		r32 radius = 0.1f;
		b32 hovering_p0 = IsPointInsideCircle(mouse.world_space_p, game_state->p0.xy, radius);
		b32 hovering_p1 = IsPointInsideCircle(mouse.world_space_p, game_state->p1.xy, radius);
		b32 hovering_p2 = IsPointInsideCircle(mouse.world_space_p, game_state->p2.xy, radius);
		b32 hovering_p3 = IsPointInsideCircle(mouse.world_space_p, game_state->p3.xy, radius);

		if(WasKeyJustPressed_bags(input, KEY_LEFT_MOUSE_BUTTON))
		{
			if(hovering_p0) game_state->point_holding = 0;
			else if(hovering_p1) game_state->point_holding = 1;
			else if(hovering_p2) game_state->point_holding = 2;
			else if(hovering_p3) game_state->point_holding = 3;
		}
		else if(!IsKeyDown_bags(input, KEY_LEFT_MOUSE_BUTTON))
		{
			game_state->point_holding = -1;
		}

		switch(game_state->point_holding)
		{
		case 0: game_state->p0 = V2_V3(mouse.world_space_p,0.0f); break;
		case 1: game_state->p1 = V2_V3(mouse.world_space_p,0.0f); break;
		case 2: game_state->p2 = V2_V3(mouse.world_space_p,0.0f); break;
		case 3: game_state->p3 = V2_V3(mouse.world_space_p,0.0f); break;
		}

		const u32 point_count = 4;
		V3 positions[] = 
		{
			game_state->p0, 
				game_state->p1, 
				game_state->p2, 
				game_state->p3
		};	
		V3 colors[] =
		{
			(hovering_p0 ? V3(1.0f,1.3f,1.3f) : V3(1.0f,0.3f,0.3f)),	
			(hovering_p1 ? V3(1.0f,1.3f,1.3f) : V3(1.0f,0.3f,0.3f)),	
			(hovering_p2 ? V3(1.0f,1.3f,1.3f) : V3(1.0f,0.3f,0.3f)),	
			(hovering_p3 ? V3(1.0f,1.3f,1.3f) : V3(1.0f,0.3f,0.3f))	
		};
		// SET_COLORS(colors,V3(1.0f,0.3f,0.3f),point_count);
		RenderSolidImmediate_bags (renderer_state, mvp, 1.0f, positions, colors, point_count, 0, 0, LINE_LOOP);
		RenderSolidImmediate_bags (renderer_state, mvp, 1.0f, positions, colors, point_count, 0, 0, POINTS_);

		V2 l0 = V2(-20.0f,-4.0f), l1 = V2(40.0f,4.0f);
		positions[0] = V2_V3(l0,0.0f);
		positions[1] = V2_V3(l1,0.0f);
		RenderSolidImmediate_bags (renderer_state, mvp, 1.0f, positions, colors, 2, 0, 0, LINE_STRIP);
		r32 t[3];
		u32 c = CubicBezierLineIntersectionTs (game_state->p0.xy, game_state->p1.xy, game_state->p2.xy, game_state->p3.xy, 
												l0, l1, t);
		if(c > 0)
		{
			V2 curve[] = 
			{
				game_state->p0.xy, 
				game_state->p1.xy, 
				game_state->p2.xy, 
				game_state->p3.xy
			};
			V2 left[4];
			V2 right[4];
			SplitCubicBezierInT(curve, t[0], left, right);
		
			if(c == 3)
			{
				u32 count = CubicBezierLineIntersectionTs(left[0], left[1], left[2], left[3], l0, l1, t);
				V2 left_[4];
				V2 right_[4];
				SplitCubicBezierInT(left, t[0], left_, right_);
				RenderBezierImmediate_bags (renderer_state, mvp, 1.0f,
											V2_V3(right_[0],0.1f),V2_V3(right_[1],0.1f),V2_V3(right_[2],0.1f),V2_V3(right_[3],0.1f), V3(0.2f,1.0f,0.2f));			
				
				count = CubicBezierLineIntersectionTs(left_[0], left_[1], left_[2], left_[3], l0, l1, t);
				V2 right__[4];
				V2 left__[4];
				SplitCubicBezierInT(left_, t[0], left__, right__);
				RenderBezierImmediate_bags (renderer_state, mvp, 1.0f,
											V2_V3(right__[0],0.1f),V2_V3(right__[1],0.1f),V2_V3(right__[2],0.1f),V2_V3(right__[3],0.1f), V3(0.2f,0.2f,1.0f));			
				RenderBezierImmediate_bags (renderer_state, mvp, 1.0f,
											V2_V3(left__[0],0.1f),V2_V3(left__[1],0.1f),V2_V3(left__[2],0.1f),V2_V3(left__[3],0.1f), V3(1.0f,1.0f,1.0f));			
			}	
			else if(c == 2)
			{
				u32 count = CubicBezierLineIntersectionTs(left[0], left[1], left[2], left[3], l0, l1, t);
				// assert(count == 1);
				V2 left_[4];
				V2 right_[4];
				SplitCubicBezierInT(left, t[0], left_, right_);
				RenderBezierImmediate_bags (renderer_state, mvp, 1.0f,
											V2_V3(left_[0],0.1f),V2_V3(left_[1],0.1f),V2_V3(left_[2],0.1f),V2_V3(left_[3],0.1f), V3(0.2f,1.0f,0.2f));
				RenderBezierImmediate_bags (renderer_state, mvp, 1.0f,
											V2_V3(right_[0],0.1f),V2_V3(right_[1],0.1f),V2_V3(right_[2],0.1f),V2_V3(right_[3],0.1f), V3(0.2f,0.2f,1.0f));			
			}
			else
			{
				RenderBezierImmediate_bags (renderer_state, mvp, 1.0f,
											V2_V3(left[0],0.1f),V2_V3(left[1],0.1f),V2_V3(left[2],0.1f),V2_V3(left[3],0.1f), V3(0.2f,1.0f,0.2f));
			}
			RenderBezierImmediate_bags (renderer_state, mvp, 1.0f,
										V2_V3(right[0],0.1f),V2_V3(right[1],0.1f),V2_V3(right[2],0.1f),V2_V3(right[3],0.1f), V3(1.2f,0.2f,0.2f));
		}*/

		char txt[128];
		sprintf(txt, "p%d[%.2f,%.2f]", 0, mouse.world_space_p.x, mouse.world_space_p.y);
		RenderText_bags(renderer_state, &renderer_state->debug_font, 
							(view_matrix * projection_matrix), 
							txt, 
							DEBUG_FONT_RES, 0.5f, mouse.world_space_p, 
							V3(1.0f,0.2f,0.2f), 1.0f, 
							&game_memory.permanent, &game_memory.transient);

		sprintf(txt, "p%d[%.2f,%.2f,%.2f]", 0, game_state->p0.x, game_state->p0.y, game_state->p0.z);
		RenderText_bags(renderer_state, &renderer_state->debug_font, 
							(view_matrix * projection_matrix), 
							txt, 
							DEBUG_FONT_RES, 0.5f, game_state->p0.xy, 
							V3(1.0f,0.2f,0.2f), 1.0f, 
							&game_memory.permanent, &game_memory.transient);
		sprintf(txt, "p%d[%.2f,%.2f,%.2f]", 1, game_state->p1.x, game_state->p1.y, game_state->p1.z);
		RenderText_bags(renderer_state, &renderer_state->debug_font, 
							(view_matrix * projection_matrix), 
							txt, 
							DEBUG_FONT_RES, 0.5f, game_state->p1.xy, 
							V3(1.0f,0.2f,0.2f), 1.0f, 
							&game_memory.permanent, &game_memory.transient);
		sprintf(txt, "p%d[%.2f,%.2f,%.2f]", 2, game_state->p2.x, game_state->p2.y, game_state->p2.z);
		RenderText_bags(renderer_state, &renderer_state->debug_font, 
							(view_matrix * projection_matrix), 
							txt, 
							DEBUG_FONT_RES, 0.5f, game_state->p2.xy, 
							V3(1.0f,0.2f,0.2f), 1.0f, 
							&game_memory.permanent, &game_memory.transient);
		sprintf(txt, "p%d[%.2f,%.2f,%.2f]", 3, game_state->p3.x, game_state->p3.y, game_state->p3.z);
		RenderText_bags(renderer_state, &renderer_state->debug_font, 
							(view_matrix * projection_matrix), 
							txt, 
							DEBUG_FONT_RES, 0.5f, game_state->p3.xy, 
							V3(1.0f,0.2f,0.2f), 1.0f, 
							&game_memory.permanent, &game_memory.transient);
	}
}
