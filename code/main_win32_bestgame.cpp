
#include <stdio.h>
#include <time.h>
#include <string.h>
#include <dsound.h>
#undef near
#undef far

#define DEBUG_BUILD true
#define OPENGL_RENDERER true
#define DIRECT_SOUND true

#include "..\..\BAGS\bags.h"

#include "bestgame.cpp"

#include "..\..\BAGS\win32_bags.cpp"

#include "..\..\BAGS\renderer_test.cpp"


LRESULT CALLBACK 
WindowProcedure(HWND window, UINT msg, WPARAM wparam, LPARAM lparam)
{
	switch(msg)
	{
		// case WM_SIZE:
		// {	
		// 	u32 width, height;
		// 	GetScreenSize_bags(GLOBAL_SYSTEM_P, &width, &height);
		// 	POINT top = {};
		// 	ClientToScreen(window, &top);
		// 	RendererResize_bags(GLOBAL_RENDERERSTATE_P, 0,0, width,height);
		// } break;

		case WM_CLOSE:
        case WM_DESTROY: 
        	GLOBAL_SYSTEM_P->quit = true;
		break;
		default: return DefWindowProc(window, msg, wparam, lparam);
	}

	return 0;
}

i32 WINAPI
WinMain (HINSTANCE instance, HINSTANCE prev_instance, LPSTR cmd, i32 cmd_show)
{
	HWND game_window = CreateWindow_bags(instance, cmd_show, WindowProcedure, INIT_WIDTH,INIT_HEIGHT);
	HDC hdc = GetDC(game_window);

	InitializeMemoryPool_bags (&game_memory.permanent, MEGABYTES(512));
	InitializeMemoryPool_bags (&game_memory.transient, MEGABYTES(512));
	InitializeMemoryPool_bags (&game_memory.assets,    MEGABYTES(512));

	GLOBAL_INPUT_P = AllocateFromMemoryPool_bags(&game_memory.permanent, Input_bags, 1);
	InitInput_bags(GLOBAL_INPUT_P);

	GLOBAL_SYSTEM_P = AllocateFromMemoryPool_bags(&game_memory.permanent, System_bags, 1);
	GLOBAL_SYSTEM_P->window = game_window;
	GLOBAL_SYSTEM_P->hdc = hdc;

	GLOBAL_GAMESTATE_P = AllocateFromMemoryPool_bags(&game_memory.permanent, GameState, 1);

	GLOBAL_ASSETMANAGERSTATE_P = AllocateFromMemoryPool_bags(&game_memory.permanent, AssetManagerState_bags, 1);
	InitAssetManager_bags(GLOBAL_ASSETMANAGERSTATE_P);
	
	// u8 *pop = PushMemoryPool_bags(&game_memory.transient);
	// {
	// 	// SGW_bags *world = AllocateFromMemoryPool_bags(&game_memory.permanent, SGW_bags, 1);
	// 	SGWBuilder_bags builder = {};
	// 	SGW_BeginBuild (&builder);

	// 	// SGWRegionsFromSVG ("data\\raw\\test_world_-1-1_+1+1.svg", &builder, {-1,-1,0}, {1,1,0});
	// 	// SGWRegionsFromSVG ("data\\raw\\test_world_0011.svg", &builder, {-1,-1,0}, {1,1,0});
	// 	SGWRegionsFromSVG ("data\\raw\\test_world_-1-111.svg", &builder, {-2,-2,0}, {2,2,0});
		
	// 	SGW_bags world = SGW_EndBuild(&builder, &game_memory.transient);
	// 	world.gravity_type = SGW_GRAVITYTYPE_WORLD;
	// 	world.world_gravity = {0,0,0};
	// 	world.gravity_magnitude = V2(0.0f,1.0f);

	// 	PackSGW(GLOBAL_ASSETMANAGERSTATE_P, world, &game_memory.transient);
	// }
	// PopMemoryPool_bags(&game_memory.transient, pop);

	GLOBAL_RENDERERSTATE_P = AllocateFromMemoryPool_bags(&game_memory.permanent, RendererState_bags, 1);
	InitRenderer_bags(GLOBAL_SYSTEM_P, GLOBAL_RENDERERSTATE_P, &game_memory.permanent, &game_memory.transient);

	GLOBAL_SOUNDSTATE_P = AllocateFromMemoryPool_bags(&game_memory.permanent, SoundState_bags, 1);
	InitSoundState_bags (GLOBAL_SOUNDSTATE_P, GLOBAL_SYSTEM_P, &game_memory.permanent);
	
	GLOBAL_TIME_P = AllocateFromMemoryPool_bags(&game_memory.permanent, Time_bags, 1);
	InitTime_bags(GLOBAL_TIME_P, TARGET_FPS); // TODO: get from hardware refresh rate?

	while(!GLOBAL_SYSTEM_P->quit)
	{
		SystemMessageLoop_bags(GLOBAL_SYSTEM_P, GLOBAL_INPUT_P);
		
		GetInput_bags(GLOBAL_SYSTEM_P, GLOBAL_INPUT_P, GLOBAL_TIME_P, &game_memory.permanent);

		if(WasKeyJustPressed_bags(GLOBAL_INPUT_P, KEY_F11)) ToggleFullscreen_bags(GLOBAL_SYSTEM_P);

		BeginRendering_bags(GLOBAL_RENDERERSTATE_P);

		// UpdateAndRenderGame(GLOBAL_INPUT_P, GLOBAL_GAMESTATE_P, GLOBAL_RENDERERSTATE_P, GLOBAL_ASSETMANAGERSTATE_P,
		// 					GLOBAL_SOUNDSTATE_P, GLOBAL_SYSTEM_P, GLOBAL_TIME_P->dt);

		RunRendererTest();

		UpdateAndRenderEditor_bags(GLOBAL_INPUT_P, GLOBAL_RENDERERSTATE_P, GLOBAL_SOUNDSTATE_P, GLOBAL_TIME_P->dt);

		OutputSound_bags (GLOBAL_SOUNDSTATE_P, &game_memory.permanent, &game_memory.transient);

		EndFrame_bags (GLOBAL_SYSTEM_P, GLOBAL_TIME_P);

		GetSoundBufferPosition(GLOBAL_SOUNDSTATE_P, &game_memory.permanent);
	}

	FreeMemoryPool_bags (&game_memory.permanent);
	FreeMemoryPool_bags (&game_memory.transient);
	FreeMemoryPool_bags (&game_memory.assets);
	ReleaseDC(game_window,hdc);
	return 0;
}