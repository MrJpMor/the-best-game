
#define GAME_TITLE "The BEST Game"
#define TARGET_FPS 60
#define INIT_WIDTH 910
#define INIT_HEIGHT 512

struct GameMemory
{
	MemoryPool_bags permanent;
	MemoryPool_bags transient;
	MemoryPool_bags assets;
};
global GameMemory game_memory;

struct Input
{
	r32 dt;

	b32 up;
	b32 down;
	b32 left;
	b32 right;
	b32 jump;

	b32 cam_up;
	b32 cam_down;
	b32 cam_left;
	b32 cam_right;
	b32 cam_zoom;

	V2 screen_pointer;
	V2 camera_pointer;
	V2 world_pointer;
	b32 pointer_down;

	b32 reset;
};

#define MOVE_WALKING_GROUND 0x1
#define MOVE_WALKING_PATH 0x2
struct MoveState
{
	r32 t;
	u16 current;
	u16 past;
};

#define HERO_WIDTH 0.25f
#define HERO_HEIGHT 0.5f
#define HERO_SIZE V2(HERO_WIDTH,HERO_HEIGHT)
#define HERO_TORSO_WIDTH (HERO_WIDTH * 0.7f)
#define HERO_TORSO_HEIGHT (HERO_HEIGHT*(1.5f*(1.0f/3.0f)))
#define HERO_FOOT_WIDTH (HERO_WIDTH*(1.0f/3.0f))
#define HERO_FOOT_HEIGHT (HERO_HEIGHT*0.1f)

struct Hero
{
	SGWPosition wp;

	V2 p;
	V2 v;
	V2 a;
	
	r32 r;

	MoveState state;

    V2 torso_p;
    V2 left_foot_p;
    V2 right_foot_p;
};

struct GameState
{
	b32 initialized;

	SGW_bags world;

	Hero hero;
	V2 gravity;

	Font *fonts;
	u32 font_count;

	u32 bgm_id;
	u32 sfx_id;
	u32 sfx_id1;

	V3 p0,p1,p2,p3;
	i32 point_holding;
};
global GameState *GLOBAL_GAMESTATE_P;