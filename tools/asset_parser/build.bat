@echo off

mkdir build\
pushd build\

del "*.exe"
del "*.obj"

..\..\ctime -begin ap_build_timings.ctm

cl /nologo /c /Foasset_parser.obj /Zi /Fdasset_parser.pdb ..\code\asset_parser.cpp
link /nologo /DEBUG /OUT:asset_parser.exe asset_parser.obj user32.lib gdi32.lib winmm.lib dxguid.lib d3d9.lib

..\..\ctime -end ap_build_timings.ctm

copy /b/v/y "asset_parser.exe" "..\..\asset_parser.exe"

popd