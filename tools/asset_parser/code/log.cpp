#include <stdarg.h>

#define LOG_FILE_NAME "asset_parse_log.txt"
global FILE *LOG;

#define Log(fmt,...) {if(!LOG){LOG = fopen(LOG_FILE_NAME, "w");}fprintf(LOG,fmt,##__VA_ARGS__);}