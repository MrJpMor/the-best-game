#include <malloc.h>
#include <stdio.h>
#include <tchar.h> 
#include <strsafe.h>

#include "..\..\..\code\bestgame_basics.h"
#include "..\..\..\code\bestgame_math.cpp"
#include "..\..\..\code\bestgame_strings.cpp"
#include "..\..\..\code\bestgame_fonts.h"
#include "..\..\..\code\bestgame_bag.h"

#include <windows.h>

struct RawFileContents
{
	void *contents;
	u32 size;
};
internal RawFileContents PlatformGetFileContents(const char *filename);
internal void PlatformFreeFileContents(RawFileContents *file);

global u32 WINDOW_WIDTH = 800;
global u32 WINDOW_HEIGHT = 600;
global b32 APP_RUNNING;

#include "log.cpp"
#include "opentype.cpp"

internal RawFileContents
PlatformGetFileContents(const char *filename)
{
	RawFileContents res = {};
	
	HANDLE file_handle = CreateFileA(filename, GENERIC_READ, FILE_SHARE_READ, 0, OPEN_EXISTING, 0, 0);
	if(file_handle != INVALID_HANDLE_VALUE)
	{
		LARGE_INTEGER file_size;
		if(GetFileSizeEx(file_handle, &file_size))
		{
			res.size = (u32)file_size.QuadPart;
			res.contents = VirtualAlloc(0, res.size, MEM_RESERVE|MEM_COMMIT, PAGE_READWRITE);
			if(res.contents)
			{
				DWORD bytes_read;
				if(ReadFile(file_handle, res.contents, res.size, &bytes_read, 0))
				{
					// 
				}
				else
				{
					MessageBox(NULL, "Failed to read file!", "Error!", MB_ICONEXCLAMATION|MB_OK);
					VirtualFree(res.contents, res.size, MEM_RELEASE);
				}
			}
			else
			{
				MessageBox(NULL, "Failed to alloc mem for file!", "Error!", MB_ICONEXCLAMATION|MB_OK);
			}
		}

		CloseHandle(file_handle);
	}
	else
	{
		MessageBox(NULL, "Failed to open file!", "Error!", MB_ICONEXCLAMATION|MB_OK);
	}

	return res;
}

internal void 
PlatformFreeFileContents(RawFileContents *file)
{
	VirtualFree(file->contents, file->size, MEM_RELEASE);
	file->contents = 0;
	file->size = 0;
}

static LRESULT CALLBACK
WindowProc(HWND wnd, UINT msg, WPARAM wparam, LPARAM lparam)
{
    switch (msg)
    {
    case WM_DESTROY:
        PostQuitMessage(0);
        return 0;

    case WM_SIZE:

        break;
    }

    return DefWindowProcW(wnd, msg, wparam, lparam);
}

struct FileList
{
    char **names;
    u32 *lengths;
    u32 count;
};

internal FileList
GetAllFilesFrom(char *path)
{
    FileList res = {};

    WIN32_FIND_DATA ffd = {};
    TCHAR szDir[MAX_PATH];
    size_t dir_length;
    HANDLE hFind = INVALID_HANDLE_VALUE;
    DWORD dwError=0;
    char *dir = Append(path,(char*)"*");

    u32 count = 0;

    dir_length = GetLength(dir);
    StringCchCopy(szDir, MAX_PATH, dir);
    free(dir);

    hFind = FindFirstFile(szDir, &ffd);
    if (INVALID_HANDLE_VALUE != hFind) 
    {
        do
        {
          if (!(ffd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY))
          {
            count++;
          }
        }
        while (FindNextFile(hFind, &ffd) != 0);

        res.names = (char**)calloc(count, sizeof(char*));
        res.lengths = (u32*)calloc(count, sizeof(u32));
        res.count = count;

        u32 i = 0;
        hFind = FindFirstFile(szDir, &ffd);
        do
        {
          if (!(ffd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY))
          {
            res.names[i] = Append(path, (char*)ffd.cFileName);
            res.lengths[i] = GetLength(res.names[i]);
            i++;
          }
        }
        while (FindNextFile(hFind, &ffd) != 0);

        FindClose(hFind);
    }

    return res;
}

internal void
FreeFileList(FileList *file_list)
{
    if(file_list->names)
    {
        for (int i = 0; i < file_list->count; ++i)
        {
            if(file_list->names[i]) free(file_list->names[i]);
        }
    }
    if(file_list->lengths) free(file_list->lengths);

    file_list->names = 0;
    file_list->lengths = 0;
    file_list->count = 0;
}

internal void
WriteOutBag(BagHeader header, void *data, u32 size, char *filename)
{
  filename = Append(filename, (char*)".bag");
  FILE *file = fopen(filename, "wb");
  free(filename);
  fwrite(&header, sizeof(BagHeader), 1, file);
  fwrite(data, size, 1, file);
  fclose(file);
}

internal u32
PackBagFontData(FontData data, u8 *memory)
{
	u32 offset = 0;

	if(data.glyphs && memory)
	{
		u8 *start = memory;

		u32 font_size = GetFontDataSize(data);
		*((u32*)memory) = font_size;
		memory += sizeof(u32);

		u32 name_length = GetLength(data.name);
		*((u32*)memory) = name_length;
		memory += sizeof(u32);

		for (u32 i = 0; i < name_length; ++i)
			*((char*)memory++) = data.name[i];

		*((i32*)memory) = data.ascent;
		memory += sizeof(i32);

		*((i32*)memory) = data.descent;
		memory += sizeof(i32);

		*((i32*)memory) = data.line_gap;
		memory += sizeof(i32);

		*((i32*)memory) = data.font_height;
		memory += sizeof(i32);

		*((u32*)memory) = data.units_per_em;
		memory += sizeof(u32);

		*((u32*)memory) = data.glyph_count;
		memory += sizeof(u32);

		for (u32 i = 0; i < data.glyph_count; ++i)
		{
			GlyphData glyph = data.glyphs[i];

			if(glyph.unicode_codepoint == 237)
			{
				glyph.unicode_codepoint = 237;	
			}

			*((i32*)memory) = glyph.unicode_codepoint;
			memory += sizeof(i32);
			
			*((u32*)memory) = glyph.vertex_count;
			memory += sizeof(u32);

			*((i32*)memory) = glyph.advance_width;
			memory += sizeof(i32);

			*((i32*)memory) = glyph.left_bearing;
			memory += sizeof(i32);

			*((i32*)memory) = glyph.min_x;
			memory += sizeof(i32);

			*((i32*)memory) = glyph.min_y;
			memory += sizeof(i32);

			*((i32*)memory) = glyph.max_x;
			memory += sizeof(i32);

			*((i32*)memory) = glyph.max_y;
			memory += sizeof(i32);

			if(glyph.vertices)
			{
				for (u32 j = 0; j < glyph.vertex_count; ++j)
				{
						*((FontVertex*)memory) = glyph.vertices[j];
						memory += sizeof(FontVertex);
				}
				free(glyph.vertices);
			}
		}

		*((u32*)memory) = data.kern_count;
		memory += sizeof(u32);

		for (u32 i = 0; i < data.kern_count; ++i)
		{
			*((KernEntry*)memory) = data.kern[i];
			memory += sizeof(KernEntry);
		}

		free(data.glyphs);
		free(data.kern);

		offset = (u32)(memory-start);
	}

	return offset;
}

i32 WINAPI
WinMain (HINSTANCE instance, HINSTANCE prev_instance, LPSTR cmd, i32 cmd_show)
{
	FileList file_list = {};
	BagHeader header = {};

	file_list = GetAllFilesFrom("fonts\\");
	FontData *font_datas = (FontData*)calloc(file_list.count, sizeof(FontData));
	u32 font_count = 0;
	for (int i = 0; i < file_list.count; ++i)
	{
		char *filename = file_list.names[i];
		if(FindSubstring(filename, (char*)".otf") ||
		FindSubstring(filename, (char*)".ttf"))
		{
			FontData font_data = ParseFont(filename);
			font_datas[font_count++] = font_data;
		}
	}
	FreeFileList(&file_list);
	if(font_count)
	{
		u32 total_size = 0;
		for (u32 i = 0; i < font_count; ++i)
			total_size += GetFontDataSize(font_datas[i]);
		u8 *packed_data = (u8*)calloc(128*1024*1024,1);
		u32 offset = 0;
		for (u32 i = 0; i < font_count; ++i)
		{
			u32 off = PackBagFontData(font_datas[i], (packed_data+offset));
			offset += off;
		}

		header.tag = TAG_FONT;
		header.count = font_count;
		WriteOutBag(header, (void*)packed_data, total_size, "..\\fonts");
		free(packed_data);
	}
	free(font_datas);
	return 0;
}