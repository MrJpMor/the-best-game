
#define SVG_MAX_NESTING 100
#define SVG_FONT_UPPER "uppercase"
#define SVG_FONT_LOWER "lowercase"
#define SVG_FONT_SYMBOLS "symbols"
#define SVG_FONT_NUMBERS "numbers"
#define SVG_MAX_SET_COUNT 27

// padrao de contagem: [0-9][0-9][0-9]*[.][0-9][0-9][0-9]*

struct Pathd
{
	V2 min;
	V2 max;
	char *src_text;
};

struct SVGFont
{
	u32 width, height;
	u32 pathd_count;
	Pathd pathds[SVG_MAX_SET_COUNT];
};



internal b32
AreEqual_ (char *a, char *b)
{
	char *c = a;
	char *d = b;
	
	while((*c!='\0')&&(*d!='\0'))
	{
		if(*c != *d) return false;
		else
		{
			c++;
			d++;
		}
	}

	return (*c == *d);
}
#define AreEqual(a,b) AreEqual_((char*)(a),(char*)(b))

internal b32
AreDifferent_ (char *a, char *b)
{
	return !AreEqual(a,b);	
}
#define AreDifferent(a,b) AreDifferent_((char*)(a),(char*)(b))

internal u32
GetLength(char *str)
{
	u32 res = 0;
	while(*(str+res)!='\0') res++;
	return res;
}

internal char *
Append(char *a, char *b)
{
	u32 a_length = GetLength(a);
	u32 b_length = GetLength(b);
	
	u32 res_length = a_length+b_length+1;
	char *res = (char*)calloc(res_length, 1);
	for (u32 i = 0; i < a_length; i++) res[i] = a[i];
	for (u32 i = 0; i < b_length; i++) res[a_length+i] = b[i];
	
	return res;
}

internal i32
GetLengthUntil(char *text, char c)
{
	char *p = text;
	while(*p!=c)
	{
		if(*p=='\0') return -1;
		else p++;
	}
	return (i32)(p - text);
}

internal char *
FindSubstring(char *str, char *sub)
{
	char *p = str;
	u32 str_length = GetLength(str);
	u32 sub_length = GetLength(sub);
	if(sub_length>str_length) return 0;

	for (u32 i = 0; i < str_length; i++)
	{
		p = str+i;
		b32 match = true;
		for (u32 j = 0; j < sub_length; j++)
		{
			if((i+j)>=str_length) break;
			if(str[i+j] != sub[j]) match = false;
		}
		if(match) return p;
		if((i+sub_length) > str_length) break;
	}

	return 0;
}

internal u32
CountSubstrings(char *str, char *sub)
{
	u32 res = 0;
	char *c = FindSubstring(str, sub);
	while(c)
	{
		res++;
		c = FindSubstring((c+1), sub);
	}
	return res;
}

internal char *
FindNthSubstring(char *str, char *sub, u32 n)
{
	char *res = 0;
	
	u32 i = 0;
	char *c = FindSubstring(str, sub);
	while(c)
	{
		if(i==n)
		{
			res = c;
			break;
		}
		else
		{
			i++;
			c = FindSubstring((c+1), sub);
		}
	}
	
	return res;
}

internal char *
SVGGetTagName(char *text)
{
	i32 lenght1 = GetLengthUntil(text+1, ' ');
	if(lenght1<0) lenght1 = GetLength(text+1);
	i32 lenght2 = GetLengthUntil(text+1, '>');
	if(lenght2<0) lenght2 = GetLength(text+1);
	u32 lenght = ((lenght1 < lenght2) ? lenght1 : lenght2)+1;
	
	char *tag_name = (char*)calloc(lenght,1);
	
	for (u32 i = 0; i < lenght-1; i++) tag_name[i] = *(text+1+i);

	return tag_name;
}

internal b32
SVGIsTagSelfEnclosed(char *text)
{
	if(*(text+1)=='/') return true;
	
	char *c = text;
	while((*(c)!='>')&&(*(c)!='\0')) c++;
	if(*(c-1)=='/') return true;

	return false;
}

internal b32
SVGIsClosureTag(char *text)
{
	if((*(text+1)=='/')) return true;
	else return false;
}

internal char *
SVGGetAttrib(char *text, char *attrib_name)
{
	char *res = 0;

	char *c = text;
	char *d = Append(attrib_name, "=\"");
	u32 d_length = GetLength(d);
	char *attrib_text = FindSubstring(text, d);
	if(attrib_text)
	{
		attrib_text += d_length;
		u32 text_length = GetLengthUntil(attrib_text, '\"')+1;

		res = (char*)calloc(text_length,1);
		for (int i = 0; i < text_length; i++) res[i] = attrib_text[i];
	}
	free(d);
	return res;
}

internal SVGFont
GetSVGFont(char *svg_text)
{
	SVGFont res = {};
	b32 proper_form = true;

	char *tag_names[SVG_MAX_NESTING] = {};
	i32 tag_names_p = 0;
	char enclosings[SVG_MAX_NESTING] = {};
	i32 enclosings_p = 0;

	char *c = svg_text;
	while(*c)
	{
		switch(*c)
		{
			case '<':
			{
				if(*(c+1)=='!')
				{
					u32 comment_length = GetLengthUntil((c+4), '-');
					c = c+4+comment_length+2;
				}
				else
				{
					char *tag_name = SVGGetTagName(c);
					if(!SVGIsTagSelfEnclosed(c) && (*(c+1)!='?'))
					{
						if(tag_names[tag_names_p]) free(tag_names[tag_names_p]);
						tag_names[tag_names_p] = tag_name;
						tag_names_p++;
					}
					else if(SVGIsClosureTag(c))
					{
						--tag_names_p;
						if(tag_names_p < 0)
							proper_form = false;
						else if(AreDifferent(tag_names[tag_names_p], (tag_name+1)))
							proper_form = false;
					}

					Log("\t-tag: %s ", tag_name);
					if(AreEqual(tag_name, "svg"))
					{
						res.width = atoi(SVGGetAttrib(c, "width"));
						res.height = atoi(SVGGetAttrib(c, "height"));
						Log("w: %u h: %u ", res.width, res.height);
					}
					else if(AreEqual(tag_name, "path"))
					{
						res.pathds[res.pathd_count++].src_text = SVGGetAttrib(c, "d");
						Log("d: %s ", res.pathds[res.pathd_count-1].src_text);
					}
					else if(AreEqual(tag_name, "/svg"))
					{
						Log("pathd_count: %u", res.pathd_count);
					}
					Log("\n");

					enclosings[enclosings_p] = *c;
					enclosings_p++;
					//free(tag_name);
				}
			}
			break;
			case '>':
			{
				if(--enclosings_p < 0)
					proper_form = false;
				else if(enclosings[enclosings_p] != '<')
					proper_form = false;
			}
			break;
		}

		c++;
	}

	if((enclosings_p != 0) || (tag_names_p!=0)) proper_form = false;

	for (u32 i = 0; i < SVG_MAX_NESTING; i++) free(tag_names[i]);
	
	if(proper_form) return res;
	else return {};
}

internal void
ParseSVGPathd(Pathd *pathd, LineSet *set)
{
	set->set_count = CountSubstrings(pathd->src_text, "M");
	Log("\t\t-set count: %u\n", set->set_count);
	set->point_count = (u32*)calloc(set->set_count,sizeof(u32));
	set->point_sets = (V2**)calloc(set->set_count,sizeof(V2*));
	for (u32 i = 0; i < set->set_count; i++)
	{
		char *M = FindNthSubstring(pathd->src_text,"M",i);
		set->point_count[i] = CountSubstrings(M,",");
		if((i+1) < set->set_count)
		{
			M = FindNthSubstring(pathd->src_text,"M",(i+1));
			set->point_count[i] -= CountSubstrings(M,",");
		}
	}
	for (u32 i = 0; i < set->set_count; i++)
	{
		char *M = FindNthSubstring(pathd->src_text,"M",i);
		Log("\t\t\t-point count: %u\n", set->point_count[i]);
		set->point_sets[i] = (V2*)calloc(set->point_count[i],sizeof(V2));
		for (u32 j = 0; j < set->point_count[i]; j++)
		{
			char *pY = FindNthSubstring(M,",",j)+1;
			char *pX = pY-1;
			while(*pX!=' ') pX--;
			V2 p = {(floorf((r32)atof(pX)*1000)/1000),(floorf((r32)atof(pY)*1000)/1000)};//{((r32)((u32)(atof(pX)))), ((r32)((u32)(atof(pY))))};
			set->point_sets[i][j] = p;
			
			if(j==0)
			{
				pathd->min = p;
				pathd->max = p;
			}
			else
			{
				if(p.x < pathd->min.x) pathd->min.x = p.x;
				else if(p.x > pathd->max.x) pathd->max.x = p.x;
				if(p.y < pathd->min.y) pathd->min.y = p.y;
				else if(p.y > pathd->max.y) pathd->max.y = p.y;
			}
		}
	}
}

internal SVGFont
ParseSVGFontFile(const char *filename, OutlineFont *font, const char *type)
{
	Log("\n-parsing \"%s\" for %s-\n", filename, type);
	FILE *svg_file = fopen(filename, "r");
	if(svg_file)
	{
		fseek(svg_file, 0, SEEK_END);
		long fsize = ftell(svg_file);
		fseek(svg_file, 0, SEEK_SET);
		char *svg_text = (char*)calloc((fsize+1), 1);
		fread(svg_text, fsize, 1, svg_file);

		r32 max_width = 0.0f;
		r32 max_height = 0.0f;

		SVGFont svg_font = GetSVGFont(svg_text);
		Log("\n\t-parsing pathds\n");
		for (u32 i = 0; i < svg_font.pathd_count; i++)
		{
			LineSet *set = 0;
			if(AreEqual(type, SVG_FONT_UPPER))
			{
				set = &font->uppercase[i];
			}
			else if(AreEqual(type, SVG_FONT_LOWER))
			{
				set = &font->lowercase[i];
			}
			else if(AreEqual(type, SVG_FONT_SYMBOLS))
			{
				set = &font->symbols[i];
			}
			else if(AreEqual(type, SVG_FONT_NUMBERS))
			{
				set = &font->numbers[i];
			}
			
			if(set)
			{
				ParseSVGPathd(&svg_font.pathds[i], set);
				
				r32 width = (svg_font.pathds[i].max.x-svg_font.pathds[i].min.x);
				r32 height = (svg_font.pathds[i].max.y-svg_font.pathds[i].min.y);
				Log("\t\t\t-w:%.3f h:%.3f\n", width,height);

				if(width > max_width) max_width = width;
				if(height > max_height) max_height = height;
			}	
		}
		Log("\t-done parsing pathds-\n");

		if(max_width > font->char_size.x) font->char_size.x = max_width;
		if(max_height > font->char_size.y) font->char_size.y = max_height;

		fclose(svg_file);
		Log("-done parsing %s-\n", type);
		return svg_font;
	}
	else
	{
		Log("\t-error opening file!");
		return {};
	}
}