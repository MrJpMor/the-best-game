
// required
global const u32 cmap = PACK_TAG('c','m','a','p');
global const u32 head = PACK_TAG('h','e','a','d');
global const u32 hhea = PACK_TAG('h','h','e','a');
global const u32 hmtx = PACK_TAG('h','m','t','x');
global const u32 maxp = PACK_TAG('m','a','x','p');
global const u32 name = PACK_TAG('n','a','m','e');
global const u32 post = PACK_TAG('p','o','s','t');
global const u32 OS2  = PACK_TAG('O','S','/','2');
// TrueType related
global const u32 cvt  = PACK_TAG('c','v','t',' ');
global const u32 fmpg = PACK_TAG('f','m','p','g');
global const u32 glyf = PACK_TAG('g','l','y','f');
global const u32 loca = PACK_TAG('l','o','c','a');
global const u32 prep = PACK_TAG('p','r','e','p');
global const u32 gasp = PACK_TAG('g','a','s','p');
// PostScript related
global const u32 CFF  = PACK_TAG('C','F','F',' ');
global const u32 CFF2 = PACK_TAG('C','F','F','2');
global const u32 VORG = PACK_TAG('V','O','R','G');
// SVG related
global const u32 SVG = PACK_TAG('S','V','G',' ');
// Bitmap Glyphs related
global const u32 EBDT = PACK_TAG('E','B','D','T');
global const u32 EBLC = PACK_TAG('E','B','L','C');
global const u32 EBSC = PACK_TAG('E','B','S','C');
global const u32 CBDT = PACK_TAG('C','B','D','T');
global const u32 CBLC = PACK_TAG('C','B','L','C');
global const u32 sbix = PACK_TAG('s','b','i','x');
// typographic
global const u32 BASE = PACK_TAG('B','A','S','E');
global const u32 GDEF = PACK_TAG('G','D','E','F');
global const u32 GPOS = PACK_TAG('G','P','O','S');
global const u32 GSUB = PACK_TAG('G','S','U','B');
global const u32 JSTF = PACK_TAG('J','S','T','F');
global const u32 MATH = PACK_TAG('M','A','T','H');
// OpenType Variations
global const u32 avar = PACK_TAG('a','v','a','r');
global const u32 cvar = PACK_TAG('c','v','a','r');
global const u32 fvar = PACK_TAG('f','v','a','r');
global const u32 gvar = PACK_TAG('g','v','a','r');
global const u32 HVAR = PACK_TAG('H','V','A','R');
global const u32 MVAR = PACK_TAG('M','V','A','R');
global const u32 VVAR = PACK_TAG('V','V','A','R');
// Color fonts related
global const u32 COLR = PACK_TAG('C','O','L','R');
global const u32 CPAL = PACK_TAG('C','P','A','L');
// Other
global const u32 DSIG = PACK_TAG('D','S','I','G');
global const u32 hdmx = PACK_TAG('h','d','m','x');
global const u32 kern = PACK_TAG('k','e','r','n');
global const u32 LTSH = PACK_TAG('L','T','S','H');
global const u32 MERG = PACK_TAG('M','E','R','G');
global const u32 meta = PACK_TAG('m','e','t','a');
global const u32 STAT = PACK_TAG('S','T','A','T');
global const u32 PLCT = PACK_TAG('P','L','C','T');
global const u32 VDMX = PACK_TAG('V','D','M','X');
global const u32 vhea = PACK_TAG('v','h','e','a');
global const u32 vmtx = PACK_TAG('v','m','t','x');

// platform_id
#define PLATFORM_ID_UNICODE 0
#define PLATFORM_ID_MAC 1
#define PLATFORM_ID_ISO 2
#define PLATFORM_ID_MICROSOFT 3
// encodingID for UNICODE
#define UNICODE_EID_UNICODE_1_0 0
#define UNICODE_EID_UNICODE_1_1 1
#define UNICODE_EID_ISO_10646 2
#define UNICODE_EID_UNICODE_2_0_BMP 3
#define UNICODE_EID_UNICODE_2_0_FULL 4
// encodingID for MICROSOFT
#define MS_EID_SYMBOL 0
#define MS_EID_UNICODE_BMP 1
#define MS_EID_SHIFTJIS 2
#define MS_EID_UNICODE_FULL 10

#define TagEquals(p,tag) ((*(u32*)p) == ((u32)tag))
global u32 VERSION_OPENTYPE_W_CFF = PACK_TAG('O','T','T','O');
global u32 VERSION_OPENTYPE_1_0 = PACK_TAG(0,1,0,0);

struct ParseBuffer
{
	u8 *data;
	i32 cursor;
	i32 size;
};

struct FontParseInfo
{
	u8 *data;
	
	ParseBuffer cff;
	ParseBuffer charstrings;
	ParseBuffer gsubrs;
	ParseBuffer subrs;
	ParseBuffer fontdicts;
	ParseBuffer fdselect;  
};

struct Charstring
{
	i32 bounds;
	i32 started;
	r32 first_x, first_y;
	r32 x, y;
	i32 min_x, max_x, min_y, max_y;

	FontVertex *vertices;
	i32 num_vertices;
};

struct GlyphData
{
	FontVertex *vertices;
	u32 vertex_count;
	i32 unicode_codepoint;
	i32 advance_width;
	i32 left_bearing;
	i32 min_x;
	i32 min_y;
	i32 max_x;
	i32 max_y;
};

struct FontData
{
	char *name;
	GlyphData *glyphs;
	KernEntry *kern;
	u32 glyph_count;
	u32 kern_count;

	i32 ascent;
	i32 descent;
	i32 line_gap;
	i32 font_height;
	u32 units_per_em;
};

inline u32
GetFontDataSize(FontData data)
{
	u32 res = 4 + 4 + 4 + 4 + 4 + 4 +4+4+4;

	res += GetLength(data.name);

	if(data.glyphs)
	{
		for (u32 i = 0; i < data.glyph_count; ++i)
		{
			res += 4+4+4+4+4+4+4+4+(data.glyphs[i].vertex_count * sizeof(FontVertex));
		}
	}

	if(data.kern)
		res += data.kern_count*sizeof(KernEntry);

	return res;
}

internal u32
GetTableOffset(u8 *data, u32 tag)
{
	u16 numTables = U16_FROM_U8P(data + 4);
	for (u16 i = 0; i < numTables; ++i)
	{
		u32 offset = 12 + (16*i);
		if(TagEquals((data+offset), tag))
			return U32_FROM_U8P(data+offset+8);
	}
	return 0;
}

internal u16
GetGlyphIndex_f4(i32 unicode_codepoint, u8 *data, u32 index_map_offset, u16 *glyph_index)
{
	b32 res = false;

	if(unicode_codepoint <= 0xffff)
	{
		u16 segcount = U16_FROM_U8P(data+index_map_offset+6) >> 1;
		u16 searchRange = U16_FROM_U8P(data+index_map_offset+8) >> 1;
		u16 entrySelector = U16_FROM_U8P(data+index_map_offset+10);
		u16 rangeShift = U16_FROM_U8P(data+index_map_offset+12) >> 1;

		// do a binary search of the segments
		u32 endCount = index_map_offset + 14;
		u32 search = endCount;

		// they lie from endCount .. endCount + segCount
		// but searchRange is the nearest power of two, so...
		if (unicode_codepoint >= U16_FROM_U8P(data + search + rangeShift*2))
			search += rangeShift*2;

		// now decrement to bias correctly to find smallest
		search -= 2;
		while (entrySelector)
		{
			u16 end;
			searchRange >>= 1;
			end = U16_FROM_U8P(data + search + searchRange*2);
			if (unicode_codepoint > end)
				search += searchRange*2;
			--entrySelector;
		}
		search += 2;

		{
			u16 offset, start;
			u16 item = (u16) ((search - endCount) >> 1);

			assert(unicode_codepoint <= U16_FROM_U8P(data + endCount + 2*item));
			start = U16_FROM_U8P(data + index_map_offset + 14 + segcount*2 + 2 + 2*item);
			if (unicode_codepoint >= start)
			{
				offset = U16_FROM_U8P(data + index_map_offset + 14 + segcount*6 + 2 + 2*item);
				if (offset == 0)
				{
					*glyph_index = (u16) (unicode_codepoint + U16_FROM_U8P(data + index_map_offset + 14 + segcount*4 + 2 + 2*item));
				}
				else
				{
					*glyph_index = U16_FROM_U8P(data + offset + (unicode_codepoint-start)*2 + index_map_offset + 14 + segcount*6 + 2 + 2*item);
				}
				res = true;
			}
		}
	}

	return res;
}

// Buffers

internal u8 
BufferGet8(ParseBuffer *b)
{
   if (b->cursor >= b->size)
      return 0;
   return b->data[b->cursor++];
}

internal u8 
BufferPeek8(ParseBuffer *b)
{
   if (b->cursor >= b->size)
      return 0;
   return b->data[b->cursor];
}

internal void 
BufferSeek(ParseBuffer *b, i32 o)
{
   assert(!(o > b->size || o < 0));
   b->cursor = (o > b->size || o < 0) ? b->size : o;
}

internal void 
BufferSkip(ParseBuffer *b, i32 o)
{
   BufferSeek(b, b->cursor + o);
}

internal u32 
BufferGet(ParseBuffer *b, i32 n)
{
   u32 v = 0;
   i32 i;
   assert(n >= 1 && n <= 4);
   for (i = 0; i < n; i++)
      v = (v << 8) | BufferGet8(b);
   return v;
}
#define BufferGet16(b)  BufferGet((b), 2)
#define BufferGet32(b)  BufferGet((b), 4)

internal ParseBuffer 
NewBuffer(const void *p, i32 size)
{
   ParseBuffer r;
   assert(size < 0x40000000);
   r.data = (u8*) p;
   r.size = (i32) size;
   r.cursor = 0;
   return r;
}

internal ParseBuffer 
BufferRange(const ParseBuffer *b, i32 o, i32 s)
{
   ParseBuffer r = NewBuffer(NULL, 0);
   if (o < 0 || s < 0 || o > b->size || s > b->size - o) return r;
   r.data = b->data + o;
   r.size = s;
   return r;
}

// CFF

internal ParseBuffer 
CffGetIndex(ParseBuffer *b)
{
   i32 count, start, offsize;
   start = b->cursor;
   count = BufferGet16(b);
   if (count) {
      offsize = BufferGet8(b);
      assert(offsize >= 1 && offsize <= 4);
      BufferSkip(b, offsize * count);
      BufferSkip(b, BufferGet(b, offsize) - 1);
   }
   return BufferRange(b, start, b->cursor - start);
}

internal u32 
CffU32(ParseBuffer *b)
{
   i32 b0 = BufferGet8(b);
   if (b0 >= 32 && b0 <= 246)       return b0 - 139;
   else if (b0 >= 247 && b0 <= 250) return (b0 - 247)*256 + BufferGet8(b) + 108;
   else if (b0 >= 251 && b0 <= 254) return -(b0 - 251)*256 - BufferGet8(b) - 108;
   else if (b0 == 28)               return BufferGet16(b);
   else if (b0 == 29)               return BufferGet32(b);
   assert(0);
   return 0;
}

internal void 
CffSkipOperand(ParseBuffer *b) 
{
   i32 v, b0 = BufferPeek8(b);
   assert(b0 >= 28);
   if (b0 == 30) 
   {
      BufferSkip(b, 1);
      while (b->cursor < b->size) 
      {
         v = BufferGet8(b);
         if ((v & 0xF) == 0xF || (v >> 4) == 0xF)
            break;
      }
   } 
   else CffU32(b);
}

internal ParseBuffer 
DictGet(ParseBuffer *b, i32 key)
{
   BufferSeek(b, 0);
   while (b->cursor < b->size) 
   {
      i32 start = b->cursor, end, op;
      while (BufferPeek8(b) >= 28)
         CffSkipOperand(b);
      end = b->cursor;
      op = BufferGet8(b);
      if (op == 12)  op = BufferGet8(b) | 0x100;
      if (op == key) return BufferRange(b, start, end-start);
   }
   return BufferRange(b, 0, 0);
}

internal void 
DictGetU32s(ParseBuffer *b, i32 key, i32 outcount, u32 *out)
{
   ParseBuffer operands = DictGet(b, key);
   for (i32 i = 0; i < outcount && operands.cursor < operands.size; i++)
      out[i] = CffU32(&operands);
}

internal i32 
CffIndexCount(ParseBuffer *b)
{
   BufferSeek(b, 0);
   return BufferGet16(b);
}

internal ParseBuffer 
CffIndexGet(ParseBuffer b, i32 i)
{
   i32 count, offsize, start, end;
   BufferSeek(&b, 0);
   count = BufferGet16(&b);
   offsize = BufferGet8(&b);
   assert(i >= 0 && i < count);
   assert(offsize >= 1 && offsize <= 4);
   BufferSkip(&b, i*offsize);
   start = BufferGet(&b, offsize);
   end = BufferGet(&b, offsize);
   return BufferRange(&b, 2+(count+1)*offsize+start, end - start);
}

internal void 
TrackVertex(Charstring *c, i32 x, i32 y)
{
	if (x > c->max_x || !c->started) c->max_x = x;
	if (y > c->max_y || !c->started) c->max_y = y;
	if (x < c->min_x || !c->started) c->min_x = x;
	if (y < c->min_y || !c->started) c->min_y = y;
	c->started = 1;
}

internal void 
SetVertex(FontVertex *v, u8 type, i32 x, i32 y, i32 cx, i32 cy)
{
	v->type = type;
	v->x = (i16) x;
	v->y = (i16) y;
	v->cx = (i16) cx;
	v->cy = (i16) cy;
}

internal void 
CharstringVertex(Charstring *c, u8 type, i32 x, i32 y, i32 cx, i32 cy, i32 cx1, i32 cy1)
{
	if (c->bounds) 
	{
		TrackVertex(c, x, y);
		if (type == vcubic) 
		{
			TrackVertex(c, cx, cy);
			TrackVertex(c, cx1, cy1);
		}
	} 
	else
	{
		SetVertex(&c->vertices[c->num_vertices], type, x, y, cx, cy);
		c->vertices[c->num_vertices].cx1 = (i16) cx1;
		c->vertices[c->num_vertices].cy1 = (i16) cy1;
	}
	c->num_vertices++;
}

internal void 
CharstringCloseShape(Charstring *ctx)
{
   if (ctx->first_x != ctx->x || ctx->first_y != ctx->y)
      CharstringVertex(ctx, vline, (i32)ctx->first_x, (i32)ctx->first_y, 0, 0, 0, 0);
}

internal void 
CharstringRMOVEto(Charstring *ctx, r32 dx, r32 dy)
{
   CharstringCloseShape(ctx);
   ctx->first_x = ctx->x = ctx->x + dx;
   ctx->first_y = ctx->y = ctx->y + dy;
   CharstringVertex(ctx, vmove, (i32)ctx->x, (i32)ctx->y, 0, 0, 0, 0);
}

internal void 
CharstringRLINEto(Charstring *ctx, r32 dx, r32 dy)
{
   ctx->x += dx;
   ctx->y += dy;
   CharstringVertex(ctx, vline, (i32)ctx->x, (i32)ctx->y, 0, 0, 0, 0);
}

internal void 
CharstringRCURVEto(Charstring *ctx, r32 dx1, r32 dy1, r32 dx2, r32 dy2, r32 dx3, r32 dy3)
{
   r32 cx1 = ctx->x + dx1;
   r32 cy1 = ctx->y + dy1;
   r32 cx2 = cx1 + dx2;
   r32 cy2 = cy1 + dy2;
   ctx->x = cx2 + dx3;
   ctx->y = cy2 + dy3;
   CharstringVertex(ctx, vcubic, (i32)ctx->x, (i32)ctx->y, (i32)cx1, (i32)cy1, (i32)cx2, (i32)cy2);
}

internal ParseBuffer 
GetSubr(ParseBuffer idx, i32 n)
{
   i32 count = CffIndexCount(&idx);
   i32 bias = 107;
   if (count >= 33900)
      bias = 32768;
   else if (count >= 1240)
      bias = 1131;
   n += bias;
   if (n < 0 || n >= count)
      return NewBuffer(NULL, 0);
   return CffIndexGet(idx, n);
}

internal ParseBuffer 
GetSubrs(ParseBuffer cff, ParseBuffer fontdict)
{
   u32 subrsoff = 0, private_loc[] = {0,0};
   ParseBuffer pdict;
   DictGetU32s(&fontdict, 18, 2, private_loc);

   if (!private_loc[1] || !private_loc[0]) return NewBuffer(NULL, 0);
   pdict = BufferRange(&cff, private_loc[1], private_loc[0]);
   DictGetU32s(&pdict, 19, 1, &subrsoff);
   if (!subrsoff) return NewBuffer(NULL, 0);
   BufferSeek(&cff, private_loc[1]+subrsoff);
   return CffGetIndex(&cff);
}

internal ParseBuffer 
CidGetGlyphSubrs(const FontParseInfo *info, i32 glyph_index)
{
	ParseBuffer fdselect = info->fdselect;
	i32 nranges, start, end, v, fmt, fdselector = -1, i;

	BufferSeek(&fdselect, 0);
	fmt = BufferGet8(&fdselect);
	if (fmt == 0) 
	{
		// untested
		BufferSkip(&fdselect, glyph_index);
		fdselector = BufferGet8(&fdselect);
	} 
	else if (fmt == 3) 
	{
	  nranges = BufferGet16(&fdselect);
	  start = BufferGet16(&fdselect);
	  for (i = 0; i < nranges; i++) 
	  {
			v = BufferGet8(&fdselect);
			end = BufferGet16(&fdselect);
			if (glyph_index >= start && glyph_index < end) 
			{
				fdselector = v;
				break;
			}
			start = end;
	  }
	}
	if (fdselector == -1) NewBuffer(NULL, 0);
	return GetSubrs(info->cff, CffIndexGet(info->fontdicts, fdselector));
}

internal i32 
GetCharstring(const FontParseInfo *info, i32 glyph_index, Charstring *c)
{
	i32 in_header = 1, maskbits = 0, subr_stack_height = 0, sp = 0, v, i, b0;
	i32 has_subrs = 0, clear_stack;
	r32 s[48];
	ParseBuffer subr_stack[10], subrs = info->subrs, b;
	r32 f;

	#define CSERROR(s) (0)

	// this currently ignores the initial width value, which isn't needed if we have hmtx
	b = CffIndexGet(info->charstrings, glyph_index);
	while (b.cursor < b.size) 
	{
		i = 0;
		clear_stack = 1;
		b0 = BufferGet8(&b);
		switch (b0) 
		{
			// @TODO implement hi32ing
			case 0x13: // hi32mask
			case 0x14: // cntrmask
				if(in_header)
				maskbits += (sp / 2); // implicit "vstem"
				in_header = 0;
				BufferSkip(&b, (maskbits + 7) / 8);
			break;

			case 0x01: // hstem
			case 0x03: // vstem
			case 0x12: // hstemhm
			case 0x17: // vstemhm
				maskbits += (sp / 2);
			break;

			case 0x15: // rmoveto
				in_header = 0;
				if (sp < 2) return CSERROR("rmoveto stack");
				CharstringRMOVEto(c, s[sp-2], s[sp-1]);
			break;
			case 0x04: // vmoveto
				in_header = 0;
				if (sp < 1) return CSERROR("vmoveto stack");
				CharstringRMOVEto(c, 0, s[sp-1]);
			break;
			case 0x16: // hmoveto
				in_header = 0;
				if (sp < 1) return CSERROR("hmoveto stack");
				CharstringRMOVEto(c, s[sp-1], 0);
			break;

			case 0x05: // rlineto
				if (sp < 2) return CSERROR("rlineto stack");
				for (; i + 1 < sp; i += 2)
					CharstringRLINEto(c, s[i], s[i+1]);
			break;

			// hlineto/vlineto and vhcurveto/hvcurveto alternate horizontal and vertical
			// starting from a different place.

			case 0x07: // vlineto
				if (sp < 1) return CSERROR("vlineto stack");
			goto vlineto;
			case 0x06: // hlineto
				if (sp < 1) return CSERROR("hlineto stack");
				for (;;) 
				{
					if (i >= sp) break;
					CharstringRLINEto(c, s[i], 0);
					i++;
					vlineto:
					if (i >= sp) break;
					CharstringRLINEto(c, 0, s[i]);
					i++;
				}
			break;

			case 0x1F: // hvcurveto
				if (sp < 4) return CSERROR("hvcurveto stack");
				goto hvcurveto;
			case 0x1E: // vhcurveto
				if (sp < 4) return CSERROR("vhcurveto stack");
				for (;;) {
				if (i + 3 >= sp) break;
				CharstringRCURVEto(c, 0, s[i], s[i+1], s[i+2], s[i+3], (sp - i == 5) ? s[i + 4] : 0.0f);
				i += 4;
			hvcurveto:
				if (i + 3 >= sp) break;
				CharstringRCURVEto(c, s[i], 0, s[i+1], s[i+2], (sp - i == 5) ? s[i+4] : 0.0f, s[i+3]);
				i += 4;
				}
			break;

			case 0x08: // rrcurveto
				if (sp < 6) return CSERROR("rcurveline stack");
				for (; i + 5 < sp; i += 6)
					CharstringRCURVEto(c, s[i], s[i+1], s[i+2], s[i+3], s[i+4], s[i+5]);
			break;

			case 0x18: // rcurveline
				if (sp < 8) return CSERROR("rcurveline stack");
				for (; i + 5 < sp - 2; i += 6)
					CharstringRCURVEto(c, s[i], s[i+1], s[i+2], s[i+3], s[i+4], s[i+5]);
				if (i + 1 >= sp) return CSERROR("rcurveline stack");
				CharstringRLINEto(c, s[i], s[i+1]);
			break;

			case 0x19: // rlinecurve
				if (sp < 8) return CSERROR("rlinecurve stack");
				for (; i + 1 < sp - 6; i += 2)
					CharstringRLINEto(c, s[i], s[i+1]);
				if (i + 5 >= sp) return CSERROR("rlinecurve stack");
				CharstringRCURVEto(c, s[i], s[i+1], s[i+2], s[i+3], s[i+4], s[i+5]);
			break;

			case 0x1A: // vvcurveto
			case 0x1B: // hhcurveto
				if (sp < 4) return CSERROR("(vv|hh)curveto stack");
				f = 0.0;
				if (sp & 1) { f = s[i]; i++; }
				for (; i + 3 < sp; i += 4) 
				{
					if (b0 == 0x1B)
						CharstringRCURVEto(c, s[i], f, s[i+1], s[i+2], s[i+3], 0.0);
					else
						CharstringRCURVEto(c, f, s[i], s[i+1], s[i+2], 0.0, s[i+3]);
					f = 0.0;
				}
			break;

			case 0x0A: // callsubr
			if (!has_subrs) 
			{
				if (info->fdselect.size)
				subrs = CidGetGlyphSubrs(info, glyph_index);
				has_subrs = 1;
			}
			// fallthrough
			case 0x1D: // callgsubr
				if (sp < 1) return CSERROR("call(g|)subr stack");
				v = (i32) s[--sp];
				if (subr_stack_height >= 10) return CSERROR("recursion limit");
				subr_stack[subr_stack_height++] = b;
				b = GetSubr(b0 == 0x0A ? subrs : info->gsubrs, v);
				if (b.size == 0) return CSERROR("subr not found");
				b.cursor = 0;
				clear_stack = 0;
			break;

			case 0x0B: // return
				if (subr_stack_height <= 0) return CSERROR("return outside subr");
				b = subr_stack[--subr_stack_height];
				clear_stack = 0;
			break;

			case 0x0E: // endchar
				CharstringCloseShape(c);
			return 1;

			case 0x0C: 
			{ // two-byte escape
				r32 dx1, dx2, dx3, dx4, dx5, dx6, dy1, dy2, dy3, dy4, dy5, dy6;
				r32 dx, dy;
				i32 b1 = BufferGet8(&b);
				switch (b1) 
				{
					// @TODO These "flex" implementations ignore the flex-depth and resolution,
					// and always draw beziers.
					case 0x22: // hflex
						if (sp < 7) return CSERROR("hflex stack");
						dx1 = s[0];
						dx2 = s[1];
						dy2 = s[2];
						dx3 = s[3];
						dx4 = s[4];
						dx5 = s[5];
						dx6 = s[6];
						CharstringRCURVEto(c, dx1, 0, dx2, dy2, dx3, 0);
						CharstringRCURVEto(c, dx4, 0, dx5, -dy2, dx6, 0);
					break;

					case 0x23: // flex
						if (sp < 13) return CSERROR("flex stack");
						dx1 = s[0];
						dy1 = s[1];
						dx2 = s[2];
						dy2 = s[3];
						dx3 = s[4];
						dy3 = s[5];
						dx4 = s[6];
						dy4 = s[7];
						dx5 = s[8];
						dy5 = s[9];
						dx6 = s[10];
						dy6 = s[11];
						//fd is s[12]
						CharstringRCURVEto(c, dx1, dy1, dx2, dy2, dx3, dy3);
						CharstringRCURVEto(c, dx4, dy4, dx5, dy5, dx6, dy6);
					break;

					case 0x24: // hflex1
						if (sp < 9) return CSERROR("hflex1 stack");
						dx1 = s[0];
						dy1 = s[1];
						dx2 = s[2];
						dy2 = s[3];
						dx3 = s[4];
						dx4 = s[5];
						dx5 = s[6];
						dy5 = s[7];
						dx6 = s[8];
						CharstringRCURVEto(c, dx1, dy1, dx2, dy2, dx3, 0);
						CharstringRCURVEto(c, dx4, 0, dx5, dy5, dx6, -(dy1+dy2+dy5));
					break;

					case 0x25: // flex1
						if (sp < 11) return CSERROR("flex1 stack");
						dx1 = s[0];
						dy1 = s[1];
						dx2 = s[2];
						dy2 = s[3];
						dx3 = s[4];
						dy3 = s[5];
						dx4 = s[6];
						dy4 = s[7];
						dx5 = s[8];
						dy5 = s[9];
						dx6 = dy6 = s[10];
						dx = dx1+dx2+dx3+dx4+dx5;
						dy = dy1+dy2+dy3+dy4+dy5;
						if (fabs(dx) > fabs(dy))
						dy6 = -dy;
						else
						dx6 = -dx;
						CharstringRCURVEto(c, dx1, dy1, dx2, dy2, dx3, dy3);
						CharstringRCURVEto(c, dx4, dy4, dx5, dy5, dx6, dy6);
					break;

					default:
						return CSERROR("unimplemented");
				}
			} break;

			default:
				if (b0 != 255 && b0 != 28 && (b0 < 32 || b0 > 254))
					return CSERROR("reserved operator");

				// push immediate
				if (b0 == 255) 
				{
					f = (r32)BufferGet32(&b) / 0x10000;
				} 
				else 
				{
					BufferSkip(&b, -1);
					f = (r32)(i16)CffU32(&b);
				}

				if (sp >= 48) return CSERROR("push stack overflow");
				s[sp++] = f;
				clear_stack = 0;
			break;
		}
		if (clear_stack) sp = 0;
	}
	return CSERROR("no endchar");

	#undef CSERROR
}

internal void 
GetGlyphHMetrics(u8 *hhea_p, u8 *hmtx_p, i32 glyph_index, i32 *advance_width, i32 *left_bearing)
{
	u16 numOfLongHorMetrics = U16_FROM_U8P(hhea_p + 34);
	if (glyph_index < numOfLongHorMetrics) 
	{
		if(advance_width)    *advance_width    = I16_FROM_U8P(hmtx_p + 4*glyph_index);
		if(left_bearing) *left_bearing = I16_FROM_U8P(hmtx_p + 4*glyph_index + 2);
	}
	else
	{
		if(advance_width)    *advance_width    = I16_FROM_U8P(hmtx_p + 4*(numOfLongHorMetrics-1));
		if(left_bearing) *left_bearing = I16_FROM_U8P(hmtx_p + 4*numOfLongHorMetrics + 2*(glyph_index - numOfLongHorMetrics));
	}
}

internal i32 
GetGlyphKernAdvance(u8 *kern, i32 glyph1, i32 glyph2)
{
	if (U16_FROM_U8P(kern+2) < 1) // number of tables, need at least 1
		return 0;
	if (U16_FROM_U8P(kern+8) != 1) // horizontal flag must be set in format
		return 0;

	i32 l = 0;
	i32 r = U16_FROM_U8P(kern+10) - 1;
	u32 needle = glyph1 << 16 | glyph2;
	while (l <= r) 
	{
		i32 m = (l + r) >> 1;
		u32 straw = U32_FROM_U8P(kern+18+(m*6)); // unaligned read
		
		if (needle < straw)
			r = m - 1;
		else if (needle > straw)
			l = m + 1;
		else
			return I16_FROM_U8P(kern+22+(m*6));
	}

	return 0;
}

internal i32 
CloseShape(FontVertex *vertices, i32 num_vertices, i32 was_off, i32 start_off,
    				i32 sx, i32 sy, i32 scx, i32 scy, i32 cx, i32 cy)
{
	if (start_off) 
	{
		if (was_off)
			SetVertex(&vertices[num_vertices++], vcurve, (cx+scx)>>1, (cy+scy)>>1, cx,cy);
		SetVertex(&vertices[num_vertices++], vcurve, sx,sy,scx,scy);
	} 
	else
	{
		if (was_off)
			SetVertex(&vertices[num_vertices++], vcurve,sx,sy,cx,cy);
		else
			SetVertex(&vertices[num_vertices++], vline,sx,sy,0,0);
	}
	return num_vertices;
}

internal i32 
GetTrueTypeGlyphShape(u8 *data, u32 glyf_offset, u32 loca_offset, i32 glyph_index, 
											u32 numGlyphs, u16 indexToLocFormat, FontVertex **pvertices, 
											i32 *min_x, i32 *min_y, i32 *max_x, i32 *max_y)
{
   i16 numberOfContours;
   u8 *endPtsOfContours;
   FontVertex *vertices = 0;
   i32 num_vertices=0;

   i32 g = -1;
 	{
		i32 g1,g2;

		if (glyph_index >= numGlyphs) return -1; // glyph index out of range
		if (indexToLocFormat >= 2) return -1; // unknown index->glyph map format

		if (indexToLocFormat == 0) 
		{
			g1 = glyf_offset + U16_FROM_U8P(data + loca_offset + glyph_index * 2) * 2;
			g2 = glyf_offset + U16_FROM_U8P(data + loca_offset + glyph_index * 2 + 2) * 2;
		} 
		else 
		{
			g1 = glyf_offset + U32_FROM_U8P (data + loca_offset + glyph_index * 4);
			g2 = glyf_offset + U32_FROM_U8P (data + loca_offset + glyph_index * 4 + 4);
		}

		g = (g1==g2 ? -1 : g1); // if length is 0, return -1
 	}

	if (g < 0) return 0;

	if(min_x) *min_x = I16_FROM_U8P(data + g + 2);
	if(min_y) *min_y = I16_FROM_U8P(data + g + 4);
	if(max_x) *max_x = I16_FROM_U8P(data + g + 6);
	if(max_y) *max_y = I16_FROM_U8P(data + g + 8);

	numberOfContours = I16_FROM_U8P(data + g);

	if (numberOfContours > 0) 
	{
		u8 flags=0,flagcount;
		i32 ins, i,j=0,m,n, next_move, was_off=0, off, start_off=0;
		i32 x,y,cx,cy,sx,sy, scx,scy;
		u8 *points;
		endPtsOfContours = (data + g + 10);
		ins = U16_FROM_U8P(data + g + 10 + numberOfContours * 2);
		points = data + g + 10 + numberOfContours * 2 + 2 + ins;

		n = 1+U16_FROM_U8P(endPtsOfContours + numberOfContours*2-2);

		m = n + 2*numberOfContours;  // a loose bound on how many vertices we might need
		vertices = (FontVertex *) calloc(m, sizeof(FontVertex));
		if (vertices == 0)
			return 0;

		next_move = 0;
		flagcount=0;

		// in first pass, we load uninterpreted data into the allocated array
		// above, shifted to the end of the array so we won't overwrite it when
		// we create our final data starting from the front

		off = m - n; // starting offset for uninterpreted data, regardless of how m ends up being calculated

		// first load flags

		for (i=0; i < n; ++i) 
		{
			if (flagcount == 0) 
			{
				flags = *points++;
				if (flags & 8)
					flagcount = *points++;
			} 
			else
				--flagcount;
			vertices[off+i].type = flags;
		}

		// now load x coordinates
		x=0;
		for (i=0; i < n; ++i) 
		{
			flags = vertices[off+i].type;
			if (flags & 2) 
			{
				i16 dx = *points++;
				x += (flags & 16) ? dx : -dx; // ???
			}
			else 
			{
				if (!(flags & 16)) 
				{
					x = x + (i16) (points[0]*256 + points[1]);
					points += 2;
				}
			}
			vertices[off+i].x = (i16) x;
		}

		// now load y coordinates
		y=0;
		for (i=0; i < n; ++i) 
		{
			flags = vertices[off+i].type;
			if (flags & 4) 
			{
				i16 dy = *points++;
				y += (flags & 32) ? dy : -dy; // ???
			} 
			else 
			{
				if (!(flags & 32)) 
				{
					y = y + (i16) (points[0]*256 + points[1]);
					points += 2;
				}
			}
			vertices[off+i].y = (i16) y;
		}

		// now convert them to our format
		num_vertices=0;
		sx = sy = cx = cy = scx = scy = 0;
		for (i=0; i < n; ++i) 
		{
			flags = vertices[off+i].type;
			x     = (i16) vertices[off+i].x;
			y     = (i16) vertices[off+i].y;

			if (next_move == i) 
			{
				if (i != 0)
				num_vertices = CloseShape(vertices, num_vertices, was_off, start_off, sx,sy,scx,scy,cx,cy);

				// now start the new one               
				start_off = !(flags & 1);
				if (start_off) 
				{
					// if we start off with an off-curve point, then when we need to find a point on the curve
					// where we can start, and we need to save some state for when we wraparound.
					scx = x;
					scy = y;
					if (!(vertices[off+i+1].type & 1)) 
					{
						// next point is also a curve point, so interpolate an on-point curve
						sx = (x + (i32) vertices[off+i+1].x) >> 1;
						sy = (y + (i32) vertices[off+i+1].y) >> 1;
					}
					else 
					{
						// otherwise just use the next point as our start point
						sx = (i32) vertices[off+i+1].x;
						sy = (i32) vertices[off+i+1].y;
						++i; // we're using point i+1 as the starting point, so skip it
					}
				}
				else 
				{
					sx = x;
					sy = y;
				}
				SetVertex(&vertices[num_vertices++], vmove,sx,sy,0,0);
				was_off = 0;
				next_move = 1 + U16_FROM_U8P(endPtsOfContours+j*2);
				++j;
			} 
			else 
			{
				if (!(flags & 1)) 
				{ // if it's a curve
					if (was_off) // two off-curve control points in a row means interpolate an on-curve midpoint
						SetVertex(&vertices[num_vertices++], vcurve, (cx+x)>>1, (cy+y)>>1, cx, cy);
					cx = x;
					cy = y;
					was_off = 1;
				} 
				else 
				{
					if (was_off)
						SetVertex(&vertices[num_vertices++], vcurve, x,y, cx, cy);
					else
						SetVertex(&vertices[num_vertices++], vline, x,y,0,0);
					was_off = 0;
				}
			}
		}
		num_vertices = CloseShape(vertices, num_vertices, was_off, start_off, sx,sy,scx,scy,cx,cy);
	} 
	else if (numberOfContours == -1) 
	{
		// Compound shapes.
		i32 more = 1;
		u8 *comp = data + g + 10;
		num_vertices = 0;
		vertices = 0;
		while (more) 
		{
			u16 flags, gidx;
			i32 comp_num_verts = 0, i;
			FontVertex *comp_verts = 0, *tmp = 0;
			r32 mtx[6] = {1,0,0,1,0,0}, m, n;

			flags = I16_FROM_U8P(comp); comp+=2;
			gidx = I16_FROM_U8P(comp); comp+=2;

			if (flags & 2) 
			{ // XY values
				if (flags & 1) 
				{ // shorts
					mtx[4] = I16_FROM_U8P(comp); comp+=2;
					mtx[5] = I16_FROM_U8P(comp); comp+=2;
				} 
				else 
				{
					mtx[4] = I8_FROM_U8P(comp); comp+=1;
					mtx[5] = I8_FROM_U8P(comp); comp+=1;
				}
			}
			else 
			{
				// @TODO handle matching point
				assert(0);
			}
			if (flags & (1<<3)) 
			{ // WE_HAVE_A_SCALE
				mtx[0] = mtx[3] = I16_FROM_U8P(comp)/16384.0f; comp+=2;
				mtx[1] = mtx[2] = 0;
			} 
			else if (flags & (1<<6)) 
			{ // WE_HAVE_AN_X_AND_YSCALE
				mtx[0] = I16_FROM_U8P(comp)/16384.0f; comp+=2;
				mtx[1] = mtx[2] = 0;
				mtx[3] = I16_FROM_U8P(comp)/16384.0f; comp+=2;
			} 
			else if (flags & (1<<7)) 
			{ // WE_HAVE_A_TWO_BY_TWO
				mtx[0] = I16_FROM_U8P(comp)/16384.0f; comp+=2;
				mtx[1] = I16_FROM_U8P(comp)/16384.0f; comp+=2;
				mtx[2] = I16_FROM_U8P(comp)/16384.0f; comp+=2;
				mtx[3] = I16_FROM_U8P(comp)/16384.0f; comp+=2;
			}

			// Find transformation scales.
			m = (r32) sqrtf(mtx[0]*mtx[0] + mtx[1]*mtx[1]);
			n = (r32) sqrtf(mtx[2]*mtx[2] + mtx[3]*mtx[3]);

			// Get indexed glyph.
			comp_num_verts = GetTrueTypeGlyphShape(data, glyf_offset, loca_offset, 
																							gidx, numGlyphs, indexToLocFormat, &comp_verts,
																							min_x, min_y, max_x, max_y);
			if (comp_num_verts > 0) 
			{
				// Transform vertices.
				for (i = 0; i < comp_num_verts; ++i) 
				{
					FontVertex *v = &comp_verts[i];
					i16 x,y;
					x=v->x; y=v->y;
					v->x = (i16)(m * (mtx[0]*x + mtx[2]*y + mtx[4]));
					v->y = (i16)(n * (mtx[1]*x + mtx[3]*y + mtx[5]));
					x=v->cx; y=v->cy;
					v->cx = (i16)(m * (mtx[0]*x + mtx[2]*y + mtx[4]));
					v->cy = (i16)(n * (mtx[1]*x + mtx[3]*y + mtx[5]));
				}
				// Append vertices.
				tmp = (FontVertex*)calloc((num_vertices+comp_num_verts),sizeof(FontVertex));
				if (!tmp)
				{
					if (vertices) free(vertices);
					if (comp_verts) free(comp_verts);
					return 0;
				}
				if (num_vertices > 0) memcpy(tmp, vertices, num_vertices*sizeof(FontVertex));
				memcpy(tmp+num_vertices, comp_verts, comp_num_verts*sizeof(FontVertex));
				if (vertices) free(vertices);
				vertices = tmp;
				free(comp_verts);
				num_vertices += comp_num_verts;
			}
			// More components ?
			more = flags & (1<<5);
		}
	} 
	else if (numberOfContours < 0) 
	{
		// @TODO other compound variations?
		assert(0);
	} 
	else 
	{
		// numberOfCounters == 0, do nothing
	}

	*pvertices = vertices;
	return num_vertices;
}

internal FontData
ParseFont(const char *filename)
{
	FontData res = {};

	Log("\n-parsing \"%s\"-\n", filename);

	RawFileContents file = PlatformGetFileContents(filename);
	FontParseInfo parse_info = {};
	u8 *data = (u8*)file.contents;

	// check if its a font
	bool is_font = (
						TagEquals(data, VERSION_OPENTYPE_W_CFF) //OpenType w\ CFF
						|| TagEquals(data, VERSION_OPENTYPE_1_0) //OpenType 1.0
						// || TagEquals(data, PACK_TAG('1',0,0,0)) //TrueType 1
						// || TagEquals(data, PACK_TAG('t','y','p','1')) || //TrueType w\ type 1
					 	// || TagEquals(data, PACK_TAG('t','r','u','e')) // Apple TrueType
					); 	
	if(is_font)
	{
		u32 tag = U32_FROM_U8P(data);
		char tag_s[] = UNPACK_TAG(tag);
		Log(" -version: %s[0x%.4x])\n", tag_s, tag);

		FontParseInfo info = {};
		info.data = data;

		u32 head_offset = GetTableOffset(data, head);
		u32 glyf_offset = GetTableOffset(data, glyf);
		u32 cff_offset = GetTableOffset(data, CFF);
		u32 loca_offset = GetTableOffset(data, loca);
		
		if(glyf_offset)
		{
			Log("  -glyf\n");
			// i16 numberOfContours = I16_FROM_U8P(data + glyf_offset);
			// Log("    -numberOfContours: %d\n", numberOfContours);
			// i16 xMin = I16_FROM_U8P(data + glyf_offset +2);
			// i16 yMin = I16_FROM_U8P(data + glyf_offset +4);
			// i16 xMax = I16_FROM_U8P(data + glyf_offset +6);
			// i16	yMax = I16_FROM_U8P(data + glyf_offset +8);
			// Log("    -min: (%d,%d)\n", xMin,yMin);
			// Log("    -max: (%d,%d)\n", xMax,yMax);
		}
		else if(cff_offset)
		{
			Log("  -CFF\n");

			ParseBuffer b, topdict, topdictidx;
			u32 cstype = 2, charstrings = 0, fdarrayoff = 0, fdselectoff = 0;

			info.fontdicts = NewBuffer(NULL, 0);
			info.fdselect = NewBuffer(NULL, 0);

			// @TODO this should use size from table (not 32MB)
			info.cff = NewBuffer(data+cff_offset, 32*1024*1024);
			b = info.cff;

			// read the header
			BufferSkip(&b, 2);
			BufferSeek(&b, BufferGet8(&b)); // hdrsize

			// @TODO the name INDEX could list multiple fonts,
			// but we just use the first one.
			CffGetIndex(&b);  // name INDEX
			topdictidx = CffGetIndex(&b);
			topdict = CffIndexGet(topdictidx, 0);
			CffGetIndex(&b);  // string INDEX
			info.gsubrs = CffGetIndex(&b);

			DictGetU32s(&topdict, 17, 1, &charstrings);
			DictGetU32s(&topdict, 0x100 | 6, 1, &cstype);
			DictGetU32s(&topdict, 0x100 | 36, 1, &fdarrayoff);
			DictGetU32s(&topdict, 0x100 | 37, 1, &fdselectoff);
			info.subrs = GetSubrs(b, topdict);

			// we only support Type 2 charstrings
			if (cstype != 2) return res;
			if (charstrings == 0) return res;

			if (fdarrayoff) 
			{
			   // looks like a CID font
			   if (!fdselectoff) return res;
			   BufferSeek(&b, fdarrayoff);
			   info.fontdicts = CffGetIndex(&b);
			   info.fdselect = BufferRange(&b, fdselectoff, b.size-fdselectoff);
			}

			BufferSeek(&b, charstrings);
			info.charstrings = CffGetIndex(&b);
		}
		else return res;

		u32 maxp_offset = GetTableOffset(data, maxp);
		u16 num_glyphs = 0;
		if(maxp_offset)
		{
			Log("  -maxp\n");
			num_glyphs = U16_FROM_U8P(data+maxp_offset+4);
			Log("    -numGlyphs: %d\n", num_glyphs);
		}
		else num_glyphs = 0xffff;

		u16 indexToLocFormat = U16_FROM_U8P(data+head_offset+50);

		u32 cmap_offset = GetTableOffset(data, cmap);
		u32 index_map_offset = 0;
		u16 platform_id = 0;
		u16 encoding_id = 0;
		if(cmap_offset)
		{
			Log("  -cmap\n");
			
			u16 num_tables = U16_FROM_U8P(data + cmap_offset + 2);
			for (u16 i=0; i < num_tables; ++i) 
			{
				u32 encoding_record = cmap_offset + 4 + 8 * i;
				// find an encoding we understand:
				u16 pid = U16_FROM_U8P(data+encoding_record);
				switch(pid)
				{
					case PLATFORM_ID_MICROSOFT:
					{
						Log("    -PLATFORM_ID_MICROSOFT\n");
						u16 eid = U16_FROM_U8P(data+encoding_record+2);
						switch (eid)
						{
							case MS_EID_UNICODE_BMP:
							{
								Log("      -MS_EID_UNICODE_BMP\n");
								platform_id = pid;
								encoding_id = eid;
								index_map_offset = cmap_offset + U32_FROM_U8P(data+encoding_record+4);
							} break;
							case MS_EID_UNICODE_FULL:
							{
								// MS/Unicode
								Log("      -MS_EID_UNICODE_FULL\n");
								platform_id = pid;
								encoding_id = eid;
								index_map_offset = cmap_offset + U32_FROM_U8P(data+encoding_record+4);
							} break;
						}
					} break;
					case PLATFORM_ID_UNICODE:
					{
						// Mac/iOS has these
						// all the encodingIDs are unicode, so we don't bother to check it
						Log("    -PLATFORM_ID_UNICODE\n");
						platform_id = pid;
						encoding_id = U16_FROM_U8P(data+encoding_record+2);
						index_map_offset = cmap_offset + U32_FROM_U8P(data+encoding_record+4);
					} break;
				}
		  }

		  {
				i32 count,string_offset;
				u8 *fc = data;
				u32 offset = 0;
				u32 nm = GetTableOffset(data, name);
				if(nm)
				{
					char *name_string = 0;
					count = U16_FROM_U8P(fc+nm+2);
					string_offset = nm + U16_FROM_U8P(fc+nm+4);
					for (i32 i=0; i < count; ++i) 
					{
						u32 loc = nm + 6 + 12 * i;
						if (0x1 == U16_FROM_U8P(fc+loc+6) && encoding_id == U16_FROM_U8P(fc+loc+2)
						    && platform_id == U16_FROM_U8P(fc+loc+0) /*&& languageID == U16_FROM_U8P(fc+loc+4)*/) 
						{
					   u32 length = U16_FROM_U8P(fc+loc+8);
					   name_string = (char *) (fc+string_offset+U16_FROM_U8P(fc+loc+10));
					   
					   if((platform_id==PLATFORM_ID_UNICODE)
					   		|| (encoding_id==MS_EID_UNICODE_FULL)
					   		|| (encoding_id==MS_EID_UNICODE_BMP))
					   {
					   	res.name = (char *)calloc(1,(length/2)+1);
					   	u32 p = 0;
					   	for (u32 j = 0; j < length; ++j)
					   	{
					   		res.name[p++] = name_string[++j];
					   	}
					   }
					   else res.name = name_string;

					   break;
						}
					}
				}
		  }
			Log("    -name:%s\n", res.name);

	  	res.glyph_count = num_glyphs;
	  	res.glyphs = (GlyphData*)calloc(num_glyphs,sizeof(GlyphData));
	  	res.kern = (KernEntry*)calloc(num_glyphs*num_glyphs,sizeof(KernEntry));
			u32 kern_count = 0;

			u32 kern_offset = GetTableOffset(data, kern);
			u32 hhea_offset = GetTableOffset(data, hhea);
			u32 hmtx_offset = GetTableOffset(data, hmtx);

	  	if(index_map_offset)
	  	{
				Log("    -index_map\n");
		  	u16 format = U16_FROM_U8P(data+index_map_offset+0);
				Log("      -format: %u\n", format);
				

				num_glyphs = 0;
				kern_count = 0;

				for (i32 unicode_codepoint = 0x0; unicode_codepoint <= 0xffff; ++unicode_codepoint)
				{
					// Log("0x%.4x\n", unicode_codepoint);

					u16 glyph_index = 0;
					b32 got_glyph = false;
					switch(format)
					{
						case 4:
							got_glyph = GetGlyphIndex_f4(unicode_codepoint, data, index_map_offset, &glyph_index);
						break;						
						default: break;
					}

					if(got_glyph) 
					{
						GlyphData glyph = {};
						
						u32 vertex_count = 0;
						if(info.cff.size) // CFF
						{
							Charstring count_ctx = {};
							count_ctx.bounds = 1;
							Charstring output_ctx = {};
							output_ctx.bounds = 0;
							if (GetCharstring(&info, glyph_index, &count_ctx))
							{
								output_ctx.vertices = (FontVertex*)calloc(count_ctx.num_vertices,sizeof(FontVertex));
								if (GetCharstring(&info, glyph_index, &output_ctx)) 
								{
									assert(output_ctx.num_vertices == count_ctx.num_vertices);
									vertex_count = output_ctx.num_vertices;
									// Log("        -0x%.4x: %c (%d)\n", glyph_index,(char)unicode_codepoint, vertex_count);
									glyph.vertices = output_ctx.vertices;
									glyph.min_x = count_ctx.min_x;
									glyph.min_y = count_ctx.min_y;
									glyph.max_x = count_ctx.max_x;
									glyph.max_y = count_ctx.max_y;
								}
							}
						}
						else // glyf
						{
							vertex_count = GetTrueTypeGlyphShape(data, glyf_offset, loca_offset, glyph_index, 
																										res.glyph_count, indexToLocFormat, &glyph.vertices,
																										&glyph.min_x, &glyph.min_y, &glyph.max_x, &glyph.max_y);
							
						}

						glyph.unicode_codepoint = unicode_codepoint;
						glyph.vertex_count = vertex_count;

						GetGlyphHMetrics((data+hhea_offset), (data+hmtx_offset), 
															glyph_index, &glyph.advance_width, &glyph.left_bearing);

						if(kern_offset)
						{
							for (i32 unicode_codepoint_2 = 0x0; unicode_codepoint_2 <= 0xffff; ++unicode_codepoint_2)
							{
								u16 glyph_id_2 = 0;
								b32 got_glyph = false;
								switch(format)
								{
									case 4:
										got_glyph = GetGlyphIndex_f4(unicode_codepoint_2, data, index_map_offset, &glyph_id_2);
									break;						
									default: break;
								}
								if(got_glyph) 
								{
									i32 kern_adv = GetGlyphKernAdvance(data+kern_offset, glyph_index, glyph_id_2);
									if(kern_adv)
									{
										// Log("      -a: %d b: %d k: %d\n",unicode_codepoint,unicode_codepoint_2,kern_adv);
										KernEntry ke = {};
										ke.glyph1_codepoint = unicode_codepoint;
										ke.glyph2_codepoint = unicode_codepoint_2;
										ke.kern_advance = kern_adv;
										res.kern[kern_count++] = ke;
									}
								}
							}
						}

						res.glyphs[num_glyphs++] = glyph;
					}
				}

				Log("        -glyph_count: %u\n",num_glyphs);
				Log("        -kern_count: %u\n", kern_count);
	  	}

   		res.font_height = I16_FROM_U8P(data+hhea_offset+4) - I16_FROM_U8P(data+hhea_offset+6);
   		res.units_per_em = U16_FROM_U8P(data+head_offset+18);
   		res.ascent   = I16_FROM_U8P(data+hhea_offset+4);
   		res.descent  = I16_FROM_U8P(data+hhea_offset+6);
   		res.line_gap = I16_FROM_U8P(data+hhea_offset+8);

	  	res.glyph_count = num_glyphs;
	  	res.kern_count = kern_count;
		}
	}
	else
	{
		u32 tag = U32_FROM_U8P(data);
		char tag_s[] = UNPACK_TAG(tag);
		Log(" -Not a font or non-supported version (%s[0x%.8x])\n", tag_s, tag);
	}
	
	PlatformFreeFileContents(&file);
	Log("-finished parsing \"%s\"-\n", filename);

	return res;
}