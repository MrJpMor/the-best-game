#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <malloc.h>

#include "bagsvf_basics.h"
#include "bagsvf_math.cpp"
#include "bagsvf_log.cpp"

struct LineSet
{
	u32 set_count;
	u32 *point_count;
	V2 **point_sets;
};

struct OutlineFont
{
	V2 char_size;
	LineSet uppercase[26];
	LineSet lowercase[26];
	LineSet symbols[27];
	LineSet numbers[10];
};

struct BAGSVHeader
{
	u32 id;
	u32 uppercase_set_count[26];
	u32 lowercase_set_count[26];
	u32 symbols_set_count[27];
	u32 numbers_set_count[10];
};

#include "bagsvf_svg.cpp"

internal void
NormalizePoints(OutlineFont *font, SVGFont svg_font, const char *type)
{
	Log("\n-normalizing points for %s-\n", type);

	Log("\t-char count: %u\n", svg_font.pathd_count);
	for (u32 set_n = 0; set_n < svg_font.pathd_count; set_n++)
	{
		Pathd pathd = svg_font.pathds[set_n];

		LineSet *set = 0;
		if(AreEqual(type, SVG_FONT_UPPER))
		{
			set = &font->uppercase[set_n];
		}
		else if(AreEqual(type, SVG_FONT_LOWER))
		{
			set = &font->lowercase[set_n];
		}
		else if(AreEqual(type, SVG_FONT_SYMBOLS))
		{
			set = &font->symbols[set_n];
		}
		else if(AreEqual(type, SVG_FONT_NUMBERS))
		{
			set = &font->numbers[set_n];
		}

		V2 size = pathd.max - pathd.min;
		V2 offset = (font->char_size - size);
		offset.x *= 0.5f;
		//Log("\t\t-pathd size: (%.1f,%.1f), offset: (%.1f,%.1f)\n", size.x,size.y,offset.x,offset.y);

		Log("\t\t-set count: %u\n", set->set_count);
		for (u32 sset_n = 0; sset_n < set->set_count; sset_n++)
		{
			
			Log("\t\t\t-points: ");
			for (u32 point_n = 0; point_n < set->point_count[sset_n]; point_n++)
			{
				V2 *p = &set->point_sets[sset_n][point_n];
				
				*p = *p - pathd.min;
				*p = *p + offset;
				*p = V2((p->x / font->char_size.x),
						-(p->y / font->char_size.y)+1.0f);
				
				Log("(%.1f,%.1f) ", p->x,p->y);
			}
			Log("\n");
		}
	}

	Log("-done normalizing %s-", type);
}

internal void
OutputOutlineFont(const char *filename, OutlineFont *font)
{
	Log("\n-outputting to \"%s\"-\n", filename);
	FILE *bagsvf_file = fopen(filename, "w+b");
	if(bagsvf_file)
	{
		font->char_size = V2(font->char_size.x/font->char_size.y,1.0f);
		Log("\t- char size: (%.1f,%.1f)\n", font->char_size.x,font->char_size.y);

		// uppercase
		for (u32 i = 0; i < 26; i++)
		{
			u32 set_count = font->uppercase[i].set_count;
			fwrite(&set_count, sizeof(u32), 1, bagsvf_file);
			for (u32 j = 0; j < set_count; j++)
			{
				u32 point_count = font->uppercase[i].point_count[j];
				fwrite(&point_count, sizeof(u32), 1, bagsvf_file);
				fwrite(font->uppercase[i].point_sets[j], sizeof(V2), point_count, bagsvf_file);
			}
		}
		// lowercase
		for (u32 i = 0; i < 26; i++)
		{
			u32 set_count = font->lowercase[i].set_count;
			fwrite(&set_count, sizeof(u32), 1, bagsvf_file);
			for (u32 j = 0; j < set_count; j++)
			{
				u32 point_count = font->lowercase[i].point_count[j];
				fwrite(&point_count, sizeof(u32), 1, bagsvf_file);
				fwrite(font->lowercase[i].point_sets[j], sizeof(V2), point_count, bagsvf_file);
			}
		}
		// symbols
		for (u32 i = 0; i < 27; i++)
		{
			u32 set_count = font->symbols[i].set_count;
			fwrite(&set_count, sizeof(u32), 1, bagsvf_file);
			for (u32 j = 0; j < set_count; j++)
			{
				u32 point_count = font->symbols[i].point_count[j];
				fwrite(&point_count, sizeof(u32), 1, bagsvf_file);
				fwrite(font->symbols[i].point_sets[j], sizeof(V2), point_count, bagsvf_file);
			}
		}
		// numbers
		for (u32 i = 0; i < 10; i++)
		{
			u32 set_count = font->numbers[i].set_count;
			fwrite(&set_count, sizeof(u32), 1, bagsvf_file);
			for (u32 j = 0; j < set_count; j++)
			{
				u32 point_count = font->numbers[i].point_count[j];
				fwrite(&point_count, sizeof(u32), 1, bagsvf_file);
				fwrite(font->numbers[i].point_sets[j], sizeof(V2), point_count, bagsvf_file);
			}
		}

		// u32 l = (u32)('2' - '0');
		// u32 p = 0;
		// fwrite(&font->numbers[l].point_count[p], sizeof(u32), 1, bagsvf_file);
		// fwrite(font->numbers[l].point_sets[p], sizeof(V2), font->numbers[l].point_count[p], bagsvf_file);

		// FILE *txtfile = fopen("text_out.txt", "w+");
		// if(txtfile)
		// {
		// 	u32 p_count = font->numbers[l].point_count[p];
		// 	V2 *points = font->numbers[l].point_sets[p];
		// 	char t[10000] = {};
		// 	char *c = t;
		// 	c += sprintf(c, "s:%u\n", p_count);
		// 	for (u32 i = 0; i < p_count; i++)
		// 	{
		// 		V2 p = *(points+i);
		// 		c += sprintf(c, "(%.1f,%.1f)\n", p.x,p.y);
		// 	}
		// 	fwrite(t, sizeof(char), 10000, txtfile);
		// 	fclose(txtfile);
		// }
		fclose(bagsvf_file);
		Log("-done-\n");
	}
	else
	{
		Log("\t-error opening file for output!\n");
	}
}

internal void
FreeOutlineFont(OutlineFont *font)
{
	for (u32 i = 0; i < 26; i++)
	{
		free(font->uppercase[i].point_count);
		for (u32 j = 0; j < font->uppercase[i].set_count; j++)
		{
			free(font->uppercase[i].point_sets[j]);
		}
		free(font->lowercase[i].point_count);
		for (u32 j = 0; j < font->lowercase[i].set_count; j++)
		{
			free(font->lowercase[i].point_sets[j]);
		}
	}
	for (u32 i = 0; i < 27; i++)
	{
		free(font->symbols[i].point_count);
		for (u32 j = 0; j < font->symbols[i].set_count; j++)
		{
			free(font->symbols[i].point_sets[j]);
		}
	}
	for (u32 i = 0; i < 10; i++)
	{
		free(font->numbers[i].point_count);
		for (u32 j = 0; j < font->numbers[i].set_count; j++)
		{
			free(font->numbers[i].point_sets[j]);
		}
	}
}

void main(int argc, char **argv)
{
	if(argc >= 6)
	{
		OutlineFont *font = (OutlineFont*)calloc(1,sizeof(OutlineFont));

		SVGFont uppercase = ParseSVGFontFile((const char *)*(argv+1), font, SVG_FONT_UPPER);
		SVGFont lowercase = ParseSVGFontFile((const char *)*(argv+2), font, SVG_FONT_LOWER);
		SVGFont symbols = ParseSVGFontFile((const char *)*(argv+3), font, SVG_FONT_SYMBOLS);
		SVGFont numbers = ParseSVGFontFile((const char *)*(argv+4), font, SVG_FONT_NUMBERS);

		NormalizePoints(font, uppercase, SVG_FONT_UPPER);
		NormalizePoints(font, lowercase, SVG_FONT_LOWER);
		NormalizePoints(font, symbols, SVG_FONT_SYMBOLS);
		NormalizePoints(font, numbers, SVG_FONT_NUMBERS);

		OutputOutlineFont((const char *)*(argv+5), font);

		FreeOutlineFont(font);
	}
	else
	{
		printf("\nargs insuficientes!\nformato correto: bagsvf.exe \"path\\to\\uppercase.svg\" \"path\\to\\lowercase.svg\" \"path\\to\\symbols.svg\" \"path\\to\\numbers.svg\" \"path\\to\\output.bagsvf\"\n");
	}
}