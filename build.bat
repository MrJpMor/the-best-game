@echo off

mkdir build\win32\debug\
pushd build\win32\debug\

del "*.exe"
del "*.obj"

..\..\..\tools\ctime -begin tbg_build_timings.ctm

cl /nologo /c /Fothebestgame_debug.obj /Zi /Fdthebestgame_debug.pdb ..\..\..\code\main_win32_bestgame.cpp
link /nologo /DEBUG /OUT:thebestgame_debug.exe thebestgame_debug.obj user32.lib gdi32.lib winmm.lib opengl32.lib

REM /SUBSYSTEM:windows /NODEFAULTLIB glew32.lib glu32.lib

goto comment
	mkdir "..\..\install_dir\"
	mkdir "..\..\install_dir\data\"
	mkdir "..\..\install_dir\data\raw\"
	mkdir "..\..\install_dir\data\shaders\"
	copy /b/v/y "thebestgame_debug.exe" "..\..\install_dir\"
	copy /b/v/y "..\..\..\data\*" "..\..\install_dir\data"
	copy /b/v/y "..\..\..\data\raw\*" "..\..\install_dir\data\raw\"
	copy /b/v/y "..\..\..\data\shaders\*" "..\..\install_dir\data\shaders\"
:comment

..\..\..\tools\ctime -end tbg_build_timings.ctm
popd