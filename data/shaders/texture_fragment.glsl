#version 330

precision highp float; // needed only for version 1.30

uniform sampler2D u_texture_sampler;
uniform float u_alpha;
uniform vec3 u_color;

in vec2 uv;

out vec4 out_Color;

void main(void)
{
	out_Color = texture(u_texture_sampler, uv);
	out_Color = vec4(out_Color.r*u_color.r, out_Color.g*u_color.g, out_Color.b*u_color.b, out_Color.a*u_alpha);
}