#version 330

uniform mat4 u_mvp;

in vec3 in_position;
in vec2 in_uv;

out vec2 uv;

void main(void)
{
	gl_Position = u_mvp * vec4(in_position, 1.0);
	uv = in_uv;
}