#version 330

precision highp float; // needed only for version 1.30

uniform float u_alpha;

in vec3 ex_color;

out vec4 out_color;

void main(void)
{
	out_color = vec4(ex_color,u_alpha);
}