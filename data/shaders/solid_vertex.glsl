#version 330

uniform mat4 u_mvp;

in vec3 in_position;
in vec3 in_color;

out vec3 ex_color;

void main(void)
{
	gl_Position = u_mvp * vec4(in_position, 1.0);
	ex_color = in_color;
}